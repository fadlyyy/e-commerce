<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Beranda_controller@index');

Route::get('/cart',function(){
//   dd(\Cart::content()); 
    $berat = 0;
    foreach(\Cart::content() as $cr){
        $qty = $cr->qty;
        $brt = $cr->options->berat;
        $berat += $qty * $brt;
    }
    dd(\Cart::content());
});

Route::get('transaksi',function(){
	\DB::beginTransaction();

	try{
		\DB::table('base64')->where('id',1)->delete();
		\App\Models\Product::where('id',1)->delete();

		dd(\DB::commit());
	}catch (\Exception $e) {
	    $aaa = \DB::rollback();
	    dd($aaa);
	}
});

Route::get('/ip',function(){
	$alamat_ip = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	dd($alamat_ip);
});

Route::get('sync',function(){
	$data = \DB::connection('toko')->select("SELECT TOP 1 * from Item");
	dd($data);
});

Auth::routes();

Route::get('/home', function(){
	return redirect('admin/beranda');
});

// Route::get('banner',function(){
// 	$product_id = \App\Models\Product::first();
// 	$uuid = \Uuid::generate(4);
// 	\DB::table('banner')->insert([
// 		'banner_id'=>$uuid,
// 		'product_id'=>$product_id->product_id,
// 	]);
// });




Route::get('detail/{id}','Beranda_controller@detail');

Route::get('kategori/{id}','Beranda_controller@kategori');

Route::get('cari','Beranda_controller@cari');

Route::get('add-to-cart/{id}','Cart_controller@add');

Route::get('keranjang','Cart_controller@index');

Route::get('keranjang/remove','Cart_controller@remove');

Route::get('ongkir/index','Cart_controller@ongkir');

Route::get('ongkir/kota/{id}','Cart_controller@kota');

Route::get('ongkir/cek','Cart_controller@cek');

Route::group(['middleware'=>'auth'],function(){
	Route::get('bayar','Cart_controller@bayar');
	Route::get('bayar/guide','Cart_controller@bayar_guide');
});

Route::get('keluar','Admin\Beranda_controller@keluar');



Route::group(['middleware'=>'auth'],function(){




	Route::group(['middleware'=>'userMiddleware'],function(){

		Route::get('user/beranda','User\Beranda_controller@index');

		// Pesanan
		Route::get('user/pesanan','User\Pesanan_controller@index');
		Route::get('user/pesanan/detail/{id}','User\Pesanan_controller@detail');
		Route::get('user/pesanan/konfirmasi','User\Pesanan_controller@konfirmasi');
		Route::get('user/pesanan/konfirmasi/{id}','User\Pesanan_controller@konfirmasi_detail');
		Route::post('user/pesanan/konfirmasi/{id}','User\Pesanan_controller@konfirmasi_proses');

		// Produk
		Route::get('user/produk/habis','User\Produk_controller@habis');
		Route::get('user/produk','User\Produk_controller@index');
		Route::get('user/produk/aktif','User\Produk_controller@aktif');
		Route::get('user/produk/nonaktif','User\Produk_controller@nonaktif');
		Route::get('user/produk/tambah','User\Produk_controller@add');
		Route::post('user/produk/tambah','User\Produk_controller@store');
		Route::get('user/produk/{id}','User\Produk_controller@edit');
		Route::put('user/produk/{id}','User\Produk_controller@update');
		Route::get('user/produk/delete/{id}','User\Produk_controller@delete');

	});








	Route::group(['middleware'=>'adminMiddleware'],function(){
		Route::get('admin/beranda','Admin\Beranda_controller@index');

		// Alamat
		Route::get('admin/alamat','Admin\Alamat_controller@edit');
		Route::put('admin/alamat','Admin\Alamat_controller@update');

		// Produk
		Route::get('admin/produk/habis','Admin\Produk_controller@habis');
		Route::get('admin/produk','Admin\Produk_controller@index');
		Route::get('admin/produk/aktif','Admin\Produk_controller@aktif');
		Route::get('admin/produk/nonaktif','Admin\Produk_controller@nonaktif');
		Route::get('admin/produk/tambah','Admin\Produk_controller@add');
		Route::post('admin/produk/tambah','Admin\Produk_controller@store');
		Route::get('admin/produk/{id}','Admin\Produk_controller@edit');
		Route::put('admin/produk/{id}','Admin\Produk_controller@update');
		Route::get('admin/produk/delete/{id}','Admin\Produk_controller@delete');


		// Kategori
		Route::get('admin/kategori','Admin\Kategori_controller@index');
		Route::get('admin/kategori/tambah','Admin\Kategori_controller@add');
		Route::post('admin/kategori/tambah','Admin\Kategori_controller@store');
		Route::get('admin/kategori/{id}','Admin\Kategori_controller@edit');
		Route::put('admin/kategori/{id}','Admin\Kategori_controller@update');
		Route::get('admin/kategori/delete/{id}','Admin\Kategori_controller@delete');

		// Warna
		Route::get('admin/warna','Admin\Warna_controller@index');
		Route::get('admin/warna/tambah','Admin\Warna_controller@add');
		Route::post('admin/warna/tambah','Admin\Warna_controller@store');
		Route::get('admin/warna/{id}','Admin\Warna_controller@edit');
		Route::put('admin/warna/{id}','Admin\Warna_controller@update');
		Route::get('admin/warna/delete/{id}','Admin\Warna_controller@delete');

		// Populer Minggu ini
		Route::get('admin/populer-minggu','Admin\Populer_minggu_controller@index');
		Route::get('admin/populer-minggu/tambah','Admin\Populer_minggu_controller@add');
		Route::post('admin/populer-minggu/tambah','Admin\Populer_minggu_controller@store');
		Route::get('admin/populer-minggu/{id}','Admin\Populer_minggu_controller@edit');
		Route::put('admin/populer-minggu/{id}','Admin\Populer_minggu_controller@update');
		Route::get('admin/populer-minggu/delete/{id}','Admin\Populer_minggu_controller@delete');

		// Featured
		Route::get('admin/featured','Admin\Featured_controller@index');
		Route::get('admin/featured/tambah','Admin\Featured_controller@add');
		Route::post('admin/featured/tambah','Admin\Featured_controller@store');
		Route::get('admin/featured/{id}','Admin\Featured_controller@edit');
		Route::put('admin/featured/{id}','Admin\Featured_controller@update');
		Route::get('admin/featured/delete/{id}','Admin\Featured_controller@delete');

		// Banner
		Route::get('admin/banner','Admin\Banner_controller@index');
		Route::get('admin/banner/{id}','Admin\Banner_controller@edit');
		Route::put('admin/banner/{id}','Admin\Banner_controller@update');

		// Best Seller
		Route::get('admin/best-seller','Admin\Best_seller_controller@index');
		Route::get('admin/best-seller/tambah','Admin\Best_seller_controller@add');
		Route::post('admin/best-seller/tambah','Admin\Best_seller_controller@store');
		// Route::get('admin/best-seller/{id}','Admin\Best_seller_controller@edit');
		// Route::put('admin/best-seller/{id}','Admin\Best_seller_controller@update');
		Route::get('admin/best-seller/delete/{id}','Admin\Best_seller_controller@delete');

		// Manage Ukuran
		Route::get('admin/ukuran','Admin\Ukuran_controller@index');
		Route::get('admin/ukuran/tambah','Admin\Ukuran_controller@add');
		Route::post('admin/ukuran/tambah','Admin\Ukuran_controller@store');
		Route::get('admin/ukuran/{id}','Admin\Ukuran_controller@edit');
		Route::put('admin/ukuran/{id}','Admin\Ukuran_controller@update');
		Route::get('admin/ukuran/delete/{id}','Admin\Ukuran_controller@delete');

		// Banner Slider
		Route::get('admin/banner-slider','Admin\Banner_slider_controller@index');
		Route::get('admin/banner-slider/tambah','Admin\Banner_slider_controller@add');
		Route::post('admin/banner-slider/tambah','Admin\Banner_slider_controller@store');
		Route::get('admin/banner-slider/{id}','Admin\Banner_slider_controller@edit');
		Route::put('admin/banner-slider/{id}','Admin\Banner_slider_controller@update');
		Route::get('admin/banner-slider/delete/{id}','Admin\Banner_slider_controller@delete');
	});
	

});
