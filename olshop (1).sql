-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `alamat`;
CREATE TABLE `alamat` (
  `alamat_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nope` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(115) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`alamat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `alamat` (`alamat_id`, `nope`, `email`, `alamat`) VALUES
('fda368c3-253c-4e27-a855-4ab9f690aa2c',	'087741465953',	'fadlyrifai95@gmail.com',	'Bekasi');

DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `banner_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `banner` (`banner_id`, `product_id`) VALUES
('f3228540-13c9-41a4-b2f5-dcfcd25ead3c',	'f04d3553-fd61-4d71-83a7-def49dfc2275');

DROP TABLE IF EXISTS `banner_slider`;
CREATE TABLE `banner_slider` (
  `banner_slider_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `urutan` int(11) NOT NULL,
  PRIMARY KEY (`banner_slider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `banner_slider` (`banner_slider_id`, `product_id`, `urutan`) VALUES
('c9a50515-82f4-489e-8a8f-627ab334eae4',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	1),
('dbdde778-b779-40fc-9ac1-2c7b8fb0567e',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	2);

DROP TABLE IF EXISTS `base64`;
CREATE TABLE `base64` (
  `id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `best_seller`;
CREATE TABLE `best_seller` (
  `best_seller_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`best_seller_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `best_seller` (`best_seller_id`, `product_id`) VALUES
('8dc290c7-74e1-4dfb-9129-3ec306a6f100',	'f04d3553-fd61-4d71-83a7-def49dfc2275');

DROP TABLE IF EXISTS `featured`;
CREATE TABLE `featured` (
  `featured_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `urutan` int(11) NOT NULL,
  PRIMARY KEY (`featured_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `featured` (`featured_id`, `product_id`, `urutan`) VALUES
('28211a3c-6744-44b3-85a2-5386405d4d2f',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	1),
('1e5d9f11-0088-4a0b-822f-a0e3b39ca8b1',	'81140209-27a9-4032-add2-642fdf8397d3',	2),
('c0b8decc-99c1-4c3f-955f-be78372d1e4d',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	3),
('13dad4ee-8a65-4981-b7ac-aa2b3516aae5',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	4),
('946c6880-f165-4605-bad2-4ce2cb42bfe4',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	5);

DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `kategori_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `kategori` (`kategori_id`, `nama`, `gambar`, `created_at`, `updated_at`) VALUES
('18a7a4d8-ce72-40a3-a138-1490d6356d3c',	'BATIK DAERAH',	'Batik Daerah.png',	'2018-12-10 23:32:47',	'2018-12-13 08:14:15'),
('3c77a05d-7ab4-4655-9825-e76d09747193',	'KULINER KHAS DAERAH',	'Makanan.png',	'2018-12-10 23:35:18',	'2018-12-13 08:44:03'),
('2baa6b6c-174c-4d92-80b7-61dd3418b1ce',	'PERTANIAN',	'Pertanian.png',	'2018-12-10 23:35:43',	'2018-12-13 08:14:57'),
('ed236a5d-3c05-476e-b227-721a633cfb19',	'KERAJINAN',	'Kerajinan.png',	'2018-12-13 08:15:22',	'2018-12-13 08:15:22'),
('ed0755b8-2f05-4adf-9e03-316c1af83e11',	'LUKISAN',	'Lukisan.png',	'2018-12-13 08:15:47',	'2018-12-13 08:15:47'),
('39569297-b822-4bbc-ac9c-5b4baafc2550',	'PAKAIAN PERIA',	'Pakaian Peria.png',	'2018-12-13 08:16:14',	'2018-12-13 08:16:14'),
('d1dd4584-91e2-4fe0-92d4-3f4be66d88b4',	'PAKAIAN WANITA',	'Pakaian Wanita.png',	'2018-12-13 08:16:30',	'2018-12-13 08:16:30'),
('c2a9a242-d889-41a4-b3b8-1589429f7f8a',	'PAKAIAN GAMIS',	'Pakaian Gamis.png',	'2018-12-13 08:16:51',	'2018-12-13 08:16:51'),
('db0443d5-bdcb-4916-ba80-6e24f941b727',	'ALAT TULIS',	'Alat Tulis.png',	'2018-12-13 08:17:08',	'2018-12-13 08:17:08'),
('ba6381df-5935-4b9f-a4bb-e9329f570321',	'DSAIN & PROGRAM',	'Dsain dan Program.png',	'2018-12-13 08:17:36',	'2018-12-13 08:17:36'),
('0ca85feb-f877-4de4-9bff-fc6ec8c732db',	'PAKAIAN OLAHRAGA',	'Baju SPort.png',	'2018-12-13 08:18:09',	'2018-12-13 08:18:09'),
('32d64b25-41a0-47b0-af0b-261d228db69f',	'SEPATU',	'Sepatu.png',	'2018-12-13 08:18:27',	'2018-12-13 08:18:27');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2019_01_20_101832_create_table_alamat',	2);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `pesanan`;
CREATE TABLE `pesanan` (
  `pesanan_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`pesanan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pesanan` (`pesanan_id`, `tanggal`, `user_id`, `total_harga`, `status`) VALUES
('9b40eaad-a290-49c2-887b-b08453f118e8',	'2018-12-23 22:33:42',	2,	390000,	1);

DROP TABLE IF EXISTS `pesanan_alamat`;
CREATE TABLE `pesanan_alamat` (
  `pesanan_detail_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `pesanan_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nama1` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `nama2` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi2` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `kota1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `kota2` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `kurir` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `kode_pos` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `nope` varchar(18) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `layanan` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ongkir` int(11) NOT NULL,
  PRIMARY KEY (`pesanan_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pesanan_alamat` (`pesanan_detail_id`, `pesanan_id`, `nama1`, `nama2`, `provinsi1`, `provinsi2`, `kota1`, `kota2`, `kurir`, `kode_pos`, `nope`, `alamat`, `layanan`, `ongkir`) VALUES
('d9176972-a401-44b6-9f0e-a931522bd4de',	'9b40eaad-a290-49c2-887b-b08453f118e8',	'fadly',	'shafa',	'14',	'12',	'44',	'61',	'jne',	'34545',	'3455642342342',	'Misal: Kp.contoh, jl. Lamin rt02/03 no.04 kel.jatiluhur kec.jatiasih Bekasi Jawa barat 17425',	'OKE',	246000);

DROP TABLE IF EXISTS `pesanan_barang`;
CREATE TABLE `pesanan_barang` (
  `pesanan_barang_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `pesanan_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `subharga` int(11) NOT NULL,
  `warna_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ukuran_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `berat` int(11) NOT NULL,
  PRIMARY KEY (`pesanan_barang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pesanan_barang` (`pesanan_barang_id`, `pesanan_id`, `product_id`, `qty`, `harga`, `subharga`, `warna_id`, `ukuran_id`, `berat`) VALUES
('49cf6c73-2efb-48ef-b98c-ae0c47d0fd58',	'9b40eaad-a290-49c2-887b-b08453f118e8',	'81140209-27a9-4032-add2-642fdf8397d3',	2,	81000,	162000,	'9f540327-a493-46ba-ba42-73d744814838',	'ea2ff971-bd88-4cc4-b2f7-3984111011a1',	450),
('037921e6-daa5-4e30-a17e-49df2e7210c4',	'9b40eaad-a290-49c2-887b-b08453f118e8',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	3,	76000,	228000,	'bfbeeb2b-9065-4576-ae8d-e25c84d8da60',	'c8c85240-4966-4bd6-8459-afc9b17b2ce1',	500);

DROP TABLE IF EXISTS `pesanan_konfirmasi`;
CREATE TABLE `pesanan_konfirmasi` (
  `pesanan_konfirmasi_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `pesanan_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pesanan_konfirmasi` (`pesanan_konfirmasi_id`, `pesanan_id`, `gambar`) VALUES
('83fc556d-c5f1-43c4-99c8-371bb43de8b1',	'9b40eaad-a290-49c2-887b-b08453f118e8',	'Asus-Zenfone-Max-pro-M2.jpg');

DROP TABLE IF EXISTS `pesanan_status`;
CREATE TABLE `pesanan_status` (
  `pesanan_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `nama` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pesanan_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `pesanan_status` (`pesanan_status_id`, `status`, `nama`) VALUES
(1,	1,	'Dikonfirmasi (Menunggu)'),
(2,	2,	'Belum Dikonfirmasi'),
(3,	3,	'Dibatalkan'),
(4,	0,	'Disetujui');

DROP TABLE IF EXISTS `populer_minggu`;
CREATE TABLE `populer_minggu` (
  `populer_minggu_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `urutan` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`populer_minggu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `populer_minggu` (`populer_minggu_id`, `product_id`, `urutan`, `created_at`, `updated_at`) VALUES
('1c47a0a8-ee8c-4461-ab23-20b732146804',	'81140209-27a9-4032-add2-642fdf8397d3',	1,	'2018-12-12 22:25:02',	'2018-12-12 22:25:02'),
('0cde2183-53d9-4a78-a885-2253f7810bb7',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	2,	'2018-12-15 05:41:24',	'2018-12-15 05:41:24');

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `kategori_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `harga_awal` int(11) NOT NULL,
  `harga_akhir` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `berat` int(11) NOT NULL,
  `dilihat` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product` (`product_id`, `kategori_id`, `nama`, `harga_awal`, `harga_akhir`, `user_id`, `stock`, `keterangan`, `berat`, `dilihat`, `status`, `discount`, `created_at`, `updated_at`) VALUES
('81140209-27a9-4032-add2-642fdf8397d3',	'39569297-b822-4bbc-ac9c-5b4baafc2550',	'Celana Sirwal',	90000,	81000,	1,	10,	'<h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">The standard Lorem Ipsum passage, used since the 1500s</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \"Open Sans\", Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \"Open Sans\", Arial, sans-serif;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>',	450,	NULL,	1,	10,	'2018-12-10 07:09:02',	'2018-12-18 09:12:49'),
('a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'3c77a05d-7ab4-4655-9825-e76d09747193',	'Kemeja Flanel',	80000,	76000,	1,	5,	'<h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\">The standard Lorem Ipsum passage, used since the 1500s</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;\"=\"\">\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>',	500,	NULL,	1,	5,	'2018-12-10 07:15:16',	'2018-12-16 10:21:42'),
('7006f917-8a3d-429e-86b1-c6b37c91b504',	'3c77a05d-7ab4-4655-9825-e76d09747193',	'SATE BANDENG KHAS BANTEN',	25000,	25000,	1,	0,	'<p>Sate Bandeng Khas Banten</p>',	0,	NULL,	1,	0,	'2018-12-13 10:11:19',	'2018-12-13 10:17:19'),
('288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'3c77a05d-7ab4-4655-9825-e76d09747193',	'Rabeg Khas Banten',	30000,	30000,	1,	20,	'<p><b>Rabeg Khas Banten</b></p><p>Selain tampilan yang menarik, rasanya juga sangat wah dilidah untuk dinikmati</p><p>Buat kalian yang belum pernah mencobanya, yuk sekarang juga pesan langsung di web ini...</p><p>jangan Bosan bosan untuk mencoba dan memsan ya :-)</p>',	0,	NULL,	1,	0,	'2018-12-13 10:17:09',	'2018-12-13 10:17:09'),
('b0784a65-3876-4c94-8be4-69d0275ad2a5',	'3c77a05d-7ab4-4655-9825-e76d09747193',	'Nasi Sum Sum Khas Banten',	15000,	15000,	1,	37,	'<p><span style=\"font-weight: 700;\">Nasi Sum Sum  Khas Banten</span></p><p>Selain tampilan yang menarik, rasanya juga sangat wah dilidah untuk dinikmati</p><p>Buat kalian yang belum pernah mencobanya, yuk sekarang juga pesan langsung di web ini...</p><p>jangan Bosan bosan untuk mencoba dan memsan ya :-)</p>',	0,	NULL,	1,	0,	'2018-12-13 10:19:18',	'2018-12-15 05:55:24'),
('2630379a-5d68-404d-bad2-81ed04800fa2',	'3c77a05d-7ab4-4655-9825-e76d09747193',	'Soto Khas Banten',	16000,	16000,	1,	0,	'<p><span style=\"font-weight: 700;\">Soto Khas Banten</span></p><p>Selain tampilan yang menarik, rasanya juga sangat wah dilidah untuk dinikmati</p><p>Buat kalian yang belum pernah mencobanya, yuk sekarang juga pesan langsung di web ini...</p><p>jangan Bosan bosan untuk mencoba dan memsan ya :-)</p>',	0,	NULL,	1,	0,	'2018-12-13 10:20:24',	'2018-12-13 10:20:24'),
('00c8b115-08e4-47d6-bbf5-e5967d64335b',	'3c77a05d-7ab4-4655-9825-e76d09747193',	'Emping Menes Banten',	15000,	15000,	1,	0,	'<p><span style=\"font-weight: 700;\">Emping Menes Banten</span></p><p>Selain tampilan yang menarik, rasanya juga sangat wah dilidah untuk dinikmati</p><p>Buat kalian yang belum pernah mencobanya, yuk sekarang juga pesan langsung di web ini...</p><p>jangan Bosan bosan untuk mencoba dan memsan ya :-)</p>',	0,	NULL,	2,	0,	'2018-12-13 10:41:36',	'2019-02-16 20:53:47'),
('8defd93e-3546-472d-822e-073c66c77a45',	'18a7a4d8-ce72-40a3-a138-1490d6356d3c',	'Batik Baduy Banten',	110000,	110000,	1,	66,	'<p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Buat kalia yang penasaran dengan produk yang 1 ini</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Yuk  sekarang juga dipesan..</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Batik Baduy Wanita Motif Keong</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">1 varian warna: biru</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">size: M, L, XL, XXL</p>',	0,	NULL,	1,	0,	'2018-12-13 11:34:48',	'2018-12-15 05:54:13'),
('632b457b-42ac-4f08-bb6a-8633aff039c2',	'18a7a4d8-ce72-40a3-a138-1490d6356d3c',	'Slayer Batik Baduy Banten',	25000,	25000,	1,	20,	'<p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\"><b>Slayer Baduy Segitiga</b></p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Varian warna: Biru, Putih, Gold, Hijau</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Ukuran: 150 x 105 x 95 centimeter</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Bahan: Katun (menyerap keringat, tidak mengkilap, warna cerah)</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Slayer Baduy segitiga lebih populer dibandingkan dengan slayer segiempat. Ukuran yang tidak terlalu besar memudahkan pengguna melilitkannya di kepala (tidak terlalu tebal).</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Banyak yang menjual slayer di luaran sana dengan harga lebih murah karena spesifikasi produknya berbeda.</p><p style=\"-webkit-tap-highlight-color: transparent; margin-bottom: 30px; line-height: 1.618; color: rgb(60, 72, 88); font-family: Roboto, Helvetica, Arial, sans-serif;\">Keunggulan slayer yang kami jual adalah&nbsp;<strong style=\"-webkit-tap-highlight-color: transparent;\">menyerap keringat</strong>&nbsp;dan tidak panas. Penggunaan slayer tidak hanya ikat kepala, melainkan menutup wajah saat berkendara sehingga spesifikasi menyerap keringat jadi sangat penting.</p>',	0,	NULL,	1,	0,	'2018-12-13 11:36:45',	'2018-12-13 11:36:45'),
('f04d3553-fd61-4d71-83a7-def49dfc2275',	'18a7a4d8-ce72-40a3-a138-1490d6356d3c',	'Kemeja Batik Pria Motif Tapak Kebo Khas Baduy Banten Warna Hijau',	110000,	110000,	1,	30,	'<p><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><b>Kemeja Batik Badu</b>y</span></p><p><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\">Kemeja Batik Pria Motif Tapak Kebo Hijau Khas Baduy Banten</span><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\">Material bahan: katun (menyerap keringat dan tidak panas)</span><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\">Jenis: printing tekstil motif batik</span><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\">Mohon chat/diskusi untuk ketersediaan ukuran karena barang dijual di MP lain dan di toko offline kami.</span></p><p><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\"><br></span></p><p><span style=\"color: rgb(96, 96, 96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 13px;\">SIZE : M, L, XL, XXL, XXXL<br></span><br></p>',	110,	NULL,	1,	0,	'2018-12-13 11:42:24',	'2018-12-13 11:42:24'),
('4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'18a7a4d8-ce72-40a3-a138-1490d6356d3c',	'Atasan Batik Wanita Motif Tapak Kebo Gold Khas Baduy Banten',	110000,	110000,	1,	55,	'<p><span style=\"color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"><b>Atasan Batik Wanuta Motif Tapak Kebo Gold Khas Baduy Banten</b></span><b><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"></b><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"><span style=\"color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\">Material bahan: katun (menyerap keringat dan tidak panas)</span><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"><span style=\"color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\">Jenis: printing tekstil motif batik</span><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"><br style=\"-webkit-font-smoothing: antialiased; color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"><span style=\"color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\">Mohon chat/diskusi untuk ketersediaan ukuran karena barang dijual di MP lain dan di toko offline kami.</span></p><p><span style=\"color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\"><br></span></p><p><span style=\"color: rgb(96, 96, 96); font-family: \"open sans\", tahoma, sans-serif; font-size: 13px;\">SIZE : XL<br></span><br></p>',	110,	NULL,	1,	0,	'2018-12-13 11:54:29',	'2018-12-15 05:53:46'),
('996ec78c-1371-46e0-b573-a797a41d81e0',	'c2a9a242-d889-41a4-b3b8-1589429f7f8a',	'Sirwal Jogger Al-Hanif Murah Berkualitas',	70000,	70000,	1,	0,	'<p><span style=\"color: rgba(0, 0, 0, 0.8); font-family: Roboto, \"Helvetica Neue\", Helvetica, Arial, \"\\\\6587泉驛正黑\", \"WenQuanYi Zen Hei\", \"Hiragino Sans GB\", \"\\\\5137黑Pro\", \"LiHei Pro\", \"Heiti TC\", \"\\\\5FAE軟正黑體\", \"Microsoft JhengHei UI\", \"Microsoft JhengHei\", sans-serif; white-space: pre-wrap;\">STOCK WARNA AKAN SELALU DI UPDATE DISINI YA :)\r\nMaaf chat di jam kerja slow respond..\r\n\r\nKondisi : Baru\r\nUkuran : All Size Dewasa (Lingkar pinggang sampai dengan +- 105cm), panjang 85cm, lingkar paha +- 65cm\r\n\r\n..\r\nWarna :\r\n- Hitam\r\n- Navy\r\n- Alice Blue / putih kebiruan\r\n- Biru Muda\r\n- Cream\r\n- Putih Susu\r\n..\r\n- Bahan Katun Twill, tebal lembut dan dingin\r\n- Pinggang Stretch (Karet), terdapat 6 buah kantong\r\n..\r\nManfaat :\r\n- Nyaman\r\n- Adem dipakai\r\n- Cocok untuk santai dan terlihat casual saat bepergian</span><br></p>',	425,	NULL,	1,	0,	'2019-02-16 14:31:05',	'2019-02-16 14:32:07');

DROP TABLE IF EXISTS `product_gambar`;
CREATE TABLE `product_gambar` (
  `product_gambar_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nama` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`product_gambar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product_gambar` (`product_gambar_id`, `product_id`, `nama`, `created_at`, `updated_at`) VALUES
('5ae24628-e37a-4e75-9db0-a95eef7cfc05',	'81140209-27a9-4032-add2-642fdf8397d3',	'14907466_98cf6664-6885-4174-9cba-d9a9e859f44b_2048_0.jpg',	'2018-12-10 07:09:03',	'2018-12-10 07:09:03'),
('8825631a-4ab3-4d26-85ce-4cd310363257',	'81140209-27a9-4032-add2-642fdf8397d3',	'sww-5b0ffba616835f60d6610293.jpg',	'2018-12-10 07:09:04',	'2018-12-10 07:09:04'),
('19d9a894-66c4-4edc-873e-5f1de0b859cc',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'download (1).jpg',	'2018-12-10 07:15:16',	'2018-12-10 07:15:16'),
('51d34b03-71e9-4961-af20-26c332bbb688',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'download.jpg',	'2018-12-10 07:15:17',	'2018-12-10 07:15:17'),
('cf5464bd-e5fb-4234-a487-05c60e690095',	'82df8e65-7fb9-4d8c-aad2-40e4db75f02f',	'download.jpg',	'2018-12-10 18:51:17',	'2018-12-10 18:51:17'),
('ebf18687-74f5-473e-8ec6-e3155d784074',	'7006f917-8a3d-429e-86b1-c6b37c91b504',	'satebandeng.jpg',	'2018-12-13 10:11:19',	'2018-12-13 10:11:19'),
('1ab9a2f9-48ef-45e7-a91f-7caeb049cffb',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'Rabeg.jpg',	'2018-12-13 10:17:09',	'2018-12-13 10:17:09'),
('bcb3ab96-8432-45a2-87a5-d45f4e45dcb5',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'nasi sum sum.jpg',	'2018-12-13 10:19:18',	'2018-12-13 10:19:18'),
('f3a68d42-daa1-4f4a-8e7e-7078deb9f64d',	'2630379a-5d68-404d-bad2-81ed04800fa2',	'soto khas banten.jpg',	'2018-12-13 10:20:24',	'2018-12-13 10:20:24'),
('6add4040-0a27-4f94-a096-3a8a41df4b97',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'emping.jpg',	'2018-12-13 10:41:36',	'2018-12-13 10:41:36'),
('3cd70fbc-c0d1-4ec8-8bf0-0c7dbe42bc91',	'8defd93e-3546-472d-822e-073c66c77a45',	'batik baduy.jpeg',	'2018-12-13 11:34:49',	'2018-12-13 11:34:49'),
('9f986c59-15f9-4c9b-a1b8-cf920c5932b5',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'slayer baduy.jpg',	'2018-12-13 11:36:45',	'2018-12-13 11:36:45'),
('c3bc0c1b-478c-4849-8649-e5f6764a6460',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'Batik Baduy Tapak Kebo Hijau.jpg',	'2018-12-13 11:42:24',	'2018-12-13 11:42:24'),
('7204846d-028e-4387-a760-2df7fdd84ca0',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'Coklat Muda.jpg',	'2018-12-13 11:54:29',	'2018-12-13 11:54:29'),
('3966bc52-394e-4540-89b0-c20aab19c427',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'hitam.png',	'2019-02-16 14:32:07',	'2019-02-16 14:32:07'),
('a0e5a94e-8824-43b6-af90-6d1f9cc02435',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'LIGHTSLATEGRAY.png',	'2019-02-16 14:32:07',	'2019-02-16 14:32:07'),
('88c063e8-f42a-42bf-a3b9-a0e391467d13',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'NAVY.png',	'2019-02-16 14:32:07',	'2019-02-16 14:32:07');

DROP TABLE IF EXISTS `product_ukuran`;
CREATE TABLE `product_ukuran` (
  `product_ukuran_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ukuran_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`product_ukuran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product_ukuran` (`product_ukuran_id`, `product_id`, `ukuran_id`) VALUES
('262e8be6-aba9-47d5-b4b6-c32f3ae21fcb',	'81140209-27a9-4032-add2-642fdf8397d3',	'ea2ff971-bd88-4cc4-b2f7-3984111011a1'),
('641d9b0d-fba1-4cbe-aa22-da3bf3c22234',	'81140209-27a9-4032-add2-642fdf8397d3',	'197c3a9b-aa02-4c83-9cff-86300c0c11ac'),
('4b278948-5aca-44b5-b9a9-80279b0bc08e',	'81140209-27a9-4032-add2-642fdf8397d3',	'18941362-91d2-44cc-8e2d-0e2d14e5b989'),
('32fc8c34-6765-4372-8343-a755c24b4da1',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'197c3a9b-aa02-4c83-9cff-86300c0c11ac'),
('f19e1568-57f1-441f-9d85-bab2f3d1b49d',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'ea2ff971-bd88-4cc4-b2f7-3984111011a1'),
('069b1f08-df6d-4e17-b93e-1ece32024bca',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'18941362-91d2-44cc-8e2d-0e2d14e5b989'),
('730351ff-d462-4b0c-bcc1-91266c02f80e',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'c8c85240-4966-4bd6-8459-afc9b17b2ce1'),
('e1d802b8-ece2-49bf-a010-909716097fde',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'c8c85240-4966-4bd6-8459-afc9b17b2ce1'),
('950ab0c2-a12f-4cc6-b16f-92be7616dfd1',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'ea2ff971-bd88-4cc4-b2f7-3984111011a1');

DROP TABLE IF EXISTS `product_warna`;
CREATE TABLE `product_warna` (
  `product_warna_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `warna_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`product_warna_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `product_warna` (`product_warna_id`, `product_id`, `warna_id`) VALUES
('14bbc0d5-77e1-4da4-a56f-313dab9bdb28',	'81140209-27a9-4032-add2-642fdf8397d3',	'9f540327-a493-46ba-ba42-73d744814838'),
('960e6445-7559-4107-a2a3-52e75c9e0cff',	'81140209-27a9-4032-add2-642fdf8397d3',	'bfbeeb2b-9065-4576-ae8d-e25c84d8da60'),
('1abf966a-0c34-4267-88bb-e91484025fb8',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'806c3691-57eb-4493-a849-cd5895e93640'),
('e41ce9a2-0fb8-4869-9c2f-7e55664f1012',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'bfbeeb2b-9065-4576-ae8d-e25c84d8da60'),
('9538928b-5d08-411f-a06a-d7049007e3b1',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('28f1430c-f859-43e6-b01e-c3ce2206e269',	'7006f917-8a3d-429e-86b1-c6b37c91b504',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('b7f61346-de9c-419a-ac25-b7937dd3fcee',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('183c0543-0836-4490-85da-5a5ecbb2bd23',	'2630379a-5d68-404d-bad2-81ed04800fa2',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('a022f7bc-e610-4df4-99b0-0890f574f588',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('0e9eb3ce-5ef0-48c8-8b68-7dbe391ae1f6',	'8defd93e-3546-472d-822e-073c66c77a45',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('4bda141f-2c65-4ae9-ab39-6110d58d6afd',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('20edce77-eb65-43ba-829f-5dc3977e4875',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('e5c5f057-5be3-4fb9-b25a-fabde16e615a',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'9f540327-a493-46ba-ba42-73d744814838'),
('4001161c-be9d-4aa4-851a-3f92f2cbf636',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'1468de91-393e-40ba-b39a-f85a4281d52d'),
('c9a0cf14-f27f-461b-96df-6ada671c4810',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'e22655d8-d888-4f5b-b794-6b535c56f1bc'),
('32a6e298-fde1-4bc3-a96e-b2a30414bd2d',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'9f540327-a493-46ba-ba42-73d744814838'),
('f5a6ab7b-a36d-4269-8f51-b5ee37fd5320',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'806c3691-57eb-4493-a849-cd5895e93640'),
('6877db72-3346-45c1-8a3e-fba119202f3f',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'bfbeeb2b-9065-4576-ae8d-e25c84d8da60'),
('37d94521-082b-4492-8f6c-38e6fa4276cb',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'e22655d8-d888-4f5b-b794-6b535c56f1bc');

DROP TABLE IF EXISTS `recent_view`;
CREATE TABLE `recent_view` (
  `recent_view_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`recent_view_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `recent_view` (`recent_view_id`, `product_id`, `created_at`) VALUES
('043bb3a2-3de2-493c-8bb5-d11643c4cf85',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-17 23:38:17'),
('7d6a3c35-6cd4-4079-b9ed-6b7c88451060',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-17 23:39:23'),
('7d470078-dc9b-4f02-9e66-cbfb45daa156',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 00:14:49'),
('c624e527-6af6-4835-bdd2-8519c48ef64e',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'2018-12-18 00:14:50'),
('f9aaf63d-66da-4414-9b80-e70f7cea4296',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2018-12-18 00:14:51'),
('a2021896-fb27-4fd3-9628-54e51866d82e',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'2018-12-18 00:14:53'),
('c65f19bf-c853-4dce-b508-18fdc05a3473',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-18 00:34:51'),
('556c262f-74fd-4b74-8e92-3ff14b6f1acb',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-18 00:49:39'),
('dfa6f834-3a58-4d75-9054-5d3f6bbd0ab2',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-18 00:50:32'),
('c98cd08d-0256-4342-861c-8731243191d2',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-18 00:51:49'),
('b266fc01-fb3d-4ca3-b6a1-fb45a2bf1669',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-18 00:52:35'),
('51666902-4fc6-4145-8090-bfa4cfcf9658',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-18 00:52:59'),
('ede76dc8-4afb-4511-87af-381f4c035acd',	'2630379a-5d68-404d-bad2-81ed04800fa2',	'2018-12-18 07:44:10'),
('14fb5ef6-c35a-48c5-8bc2-e3dd066778e7',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:01:07'),
('03075f01-ab69-4d81-bdb7-b9e7ccd6324b',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:03:18'),
('14c01e26-1d8d-44cb-be83-fdcf3def41db',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:08:39'),
('a629bccb-d123-4a30-9727-d87804c59926',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:09:44'),
('262b0c38-8270-43d2-a7a3-d5ec0f9226c0',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:11:08'),
('cd20de36-31d2-4935-bfff-44015f1abb83',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:13:18'),
('2336add0-b906-4781-a8b0-44e572994748',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:14:24'),
('96792fe1-2c13-46c3-b9e7-e6ea644bde0d',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-18 09:25:03'),
('ac3a5d03-615f-4688-be28-0f5e3f6f8d1a',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:40:53'),
('a82be42b-a937-4ab1-b4a1-fee5b64a531b',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:45:08'),
('273cb3b0-5956-4204-ab0a-99e1a9262a24',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:48:09'),
('91c16f54-35b0-400a-8c71-ab8beedbc0cc',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:48:41'),
('8ed2a134-7df8-45af-aad8-5d63368787a5',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:49:50'),
('8686ab53-e79b-4538-b801-ff5e4d36ad93',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:54:42'),
('ad520ced-44c0-4616-916a-dee9745c7f44',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:56:01'),
('0738e69f-8f4f-4cb8-9deb-70d05be7bffa',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:58:32'),
('e251ef2b-c22b-4ec5-9d43-0468ce4106a9',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 14:59:35'),
('637975a9-82a9-4166-9c5a-b36c576ae21a',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:01:44'),
('4689bbad-f43d-467c-be2c-73271df009d2',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:05:56'),
('65070de5-c535-44e9-8f2b-5f07d780d1b6',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:35:36'),
('44d592b1-09aa-42f6-8924-0008e47cfa0d',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:36:14'),
('a86ce5ff-e69c-439f-8ff4-eb246eb986cb',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:37:51'),
('bb4a5292-9974-4dc6-81de-20afbf140530',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:39:19'),
('ea2c3425-9cc3-4a2e-843f-e5720d0e7d04',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:40:05'),
('48734281-ea1b-45dd-81c2-b3bb0b885753',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 15:40:57'),
('5098cc99-43b9-4623-9b7c-386e414ecf98',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 22:43:19'),
('c7cd7252-f588-4b89-9448-a976ad353b2a',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 22:46:45'),
('5f64e01f-7d9b-441e-b64c-07ece8bd09e4',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 22:47:25'),
('e284baa1-0787-42d2-9bc8-677396580ce2',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 22:48:34'),
('d43e3def-6ad9-4b55-8064-f90f5c6ee167',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 22:49:34'),
('e91d6eb6-63b2-4e80-a158-0845cc6a4421',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-18 22:50:38'),
('08b28d78-fadf-4070-a0e1-d674300f5a7f',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 06:02:19'),
('705cd60c-b6d4-4b5e-9c7f-0d590b6f9a96',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 06:02:39'),
('0bc460cd-78d4-407f-95f7-206532c702c6',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 06:05:06'),
('91285dae-ee73-4813-9c29-87a00ff8aaf2',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 06:07:15'),
('fcd16fe3-5941-4aba-af69-35069fbc9731',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 06:07:28'),
('b98845dc-7576-4a2c-8b14-12b5cae4f573',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 06:08:22'),
('08ab24e4-9228-4601-9179-b9644a02a5c6',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 06:09:02'),
('67a13081-ce48-4266-9611-e5fdb3704cd0',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 06:09:38'),
('3d33dd36-31a1-44b0-989b-8a35913d53e8',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 06:17:58'),
('6df58b80-d196-4cf8-b4a3-6ccae1bd6599',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 06:18:12'),
('86182c9d-51ec-4db3-b6d0-847c4aea5df1',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:23:42'),
('10715f5c-da8d-40a1-9099-7d7caddec578',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:24:06'),
('8bda80b5-1b9b-4e52-860c-2a4dbce3a0bd',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:25:09'),
('9ab91f70-f83b-432b-be31-e24efc1463a4',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:25:41'),
('8cd716fb-69e5-44cc-b4ae-94a4f1602c83',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:26:11'),
('cd88c974-f5a6-43e4-a63d-668a12ec9bdd',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:26:39'),
('707429de-d403-4522-8770-57bab12f7455',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:27:15'),
('7ed1f967-3ca4-4bca-9401-38e6018cbfc8',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:54:55'),
('a489c1b1-08a5-4c0e-8910-380e24b32d34',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 06:56:12'),
('f9ed5c47-69df-4543-855d-df019fce7cde',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:57:03'),
('1dd2d955-ca1a-4d68-8bae-8575ced3745f',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 06:57:41'),
('d0472245-745f-4279-8864-b13480cb504d',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 07:05:47'),
('13951705-3a65-433f-b1d1-15f1c8e2eab0',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 07:06:16'),
('425e524a-763f-4d47-b430-50cb42840c6e',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 07:06:56'),
('44adcace-463d-4143-bc9a-4d87b70635f7',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 07:07:24'),
('411c1feb-f9c2-42fd-8a1d-36f751b70357',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'2018-12-19 07:09:56'),
('64f2911b-611e-4e55-be85-fd859e5b613e',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'2018-12-19 07:10:50'),
('4d679a94-764e-4f86-86f0-a11606324c17',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'2018-12-19 07:14:15'),
('55c19ec8-a5da-4ab9-a05a-60ddf0f8aebc',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 07:14:50'),
('daa4bb72-cf07-4b7d-86fe-69181f0a7de9',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 07:15:02'),
('81c681bf-45c0-4283-a7d3-1d3594ce2f76',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 07:15:19'),
('30cd2802-a3ed-4070-8c39-acfb35a47861',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 07:22:25'),
('955b7097-09f4-4007-86da-d0853c82d48e',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2018-12-19 07:22:32'),
('9c35a86f-55b8-46f0-8b6d-cc662c1e2ee0',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2018-12-19 07:38:54'),
('2f302bdf-f41b-4014-9c37-1ff283d6413b',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2018-12-19 07:39:05'),
('69fbed6e-6140-4763-9842-57264fdf03fb',	'7006f917-8a3d-429e-86b1-c6b37c91b504',	'2018-12-19 08:17:21'),
('1525241a-6d9f-4cc9-932f-be7049184166',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 09:47:03'),
('b81a2105-c57d-44c5-9242-2e1f4cab11bd',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 09:51:21'),
('39ada7da-6ab4-4044-9f44-d83969697c21',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 09:51:33'),
('2cc43baa-f774-4a65-bc4b-70c030c82438',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 09:57:38'),
('c7dd632a-7eb9-44b8-93e8-daac511ff1c6',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 09:57:44'),
('8be477cf-f61d-4699-a90c-7ef5d7f03b34',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 09:57:52'),
('0f34c39f-014c-48ff-ac44-99b5d2cb6984',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 09:58:33'),
('940937b4-fc4e-4c7a-a8c7-ab10698c74d1',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 09:58:39'),
('142f0583-e734-41e6-a0df-b0d6156185fd',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 09:58:50'),
('2f298bbf-8252-4c15-95ea-2eb3bedce6fa',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 09:59:03'),
('2804ff4f-ea28-4f86-9254-f409444d4d71',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 10:03:46'),
('07e414ae-4d46-4fd9-aa9f-d42bab644b74',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 10:04:00'),
('ceed0f86-8a5c-49e6-9831-4872551bb2df',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 10:04:06'),
('e6578dca-b889-4b42-8d2d-2e9730a2ba62',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 10:05:05'),
('b2cc3a4c-12ac-4a74-a935-67accbd7a0d0',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 10:19:12'),
('76bb9075-3086-4a40-bd69-c8e290a30e18',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 10:20:48'),
('71e85269-a1c3-4ae4-8049-39076da04634',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 10:21:05'),
('eb67be5b-e133-4f63-9ea6-2f26234c94a1',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 10:21:33'),
('2a512e16-4910-4bb0-85e8-3c0313943bc9',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 10:22:02'),
('10405383-854f-495d-b1fb-176504832d0a',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 10:24:03'),
('15021393-6f0b-4847-bb3b-56e0f8236f77',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 10:24:10'),
('b7d1cc26-6157-48f6-b8cd-1387353b6e21',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 10:40:39'),
('d774eacd-e60d-4658-a259-83dda806f96a',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-19 10:57:32'),
('81f0a4ff-9972-4a96-b98d-793c246d44e2',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 10:58:19'),
('07bb1fc4-4df6-44d9-9c28-f9ec339808ab',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'2018-12-19 11:47:00'),
('80b34630-e300-42ec-87d4-2dfcdba8ff71',	'f04d3553-fd61-4d71-83a7-def49dfc2275',	'2018-12-19 11:47:14'),
('8ab8b818-46c5-4aa4-a1d2-2d73f1c16c60',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-19 14:33:16'),
('7bede02b-366a-4374-bafd-c7bd37ec0a59',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'2018-12-19 14:51:41'),
('1b88f4cc-fa7c-4cf5-a178-0faa69097c2c',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'2018-12-19 14:52:20'),
('384c8106-6b64-472e-9abb-1f1589c012c2',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'2018-12-19 14:52:38'),
('8f325177-5f3d-4496-9376-d11805faab50',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'2018-12-19 14:52:52'),
('8c0fb07a-8fd4-4ec4-bd3a-c6fce739c650',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 15:04:29'),
('6b94758c-6804-48f9-9d21-ed0b7258b321',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 15:05:10'),
('c1e9c421-79c9-4070-815d-724be84b0d61',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-19 20:53:06'),
('70f44d84-e052-4844-be1a-dc6f19b0ba7c',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 20:54:40'),
('2e29aed8-12cc-46dc-88d1-8f104dd050d7',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 20:55:12'),
('f4456aa4-19ed-4f74-baf2-1a902d94396e',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 20:56:00'),
('fbd90177-ce23-4c37-aba9-17487f850df9',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-19 20:56:24'),
('b4fda1aa-4b69-41ab-ae86-7cafffbc991c',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-20 20:18:12'),
('2be6ca20-e13a-470f-88f8-8f1711d7a384',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-20 20:18:28'),
('130298fc-679c-4cd7-8e32-13fc377b1a8e',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-20 20:18:38'),
('1d0b6ab3-62d6-40d0-938e-79eb10ac0365',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-20 20:18:49'),
('8cd07deb-92a8-46a7-8b91-bce96549ef26',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-20 20:19:06'),
('7f7b685d-e30b-4eb1-917f-ae2810a7b68c',	'8defd93e-3546-472d-822e-073c66c77a45',	'2018-12-20 21:43:08'),
('bc66ebe0-746b-4375-b722-88a66bc0d0d3',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-21 06:05:42'),
('8bdd4207-f7e9-4530-933e-d96d28317e7b',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-21 06:05:49'),
('ee9c8453-fa97-4646-95ad-c9570423af4f',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-21 06:05:58'),
('d7a34bfb-403a-4e43-bb5a-7586fabcc45a',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-21 06:25:25'),
('41fe4aef-0842-46c6-967f-ddc848eaa420',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-21 07:00:23'),
('0c9b5a43-fc4a-488d-bfd6-70adefff92db',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 07:10:40'),
('8e735987-a468-4276-a6bd-21d362764ab1',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 07:10:49'),
('2db7a54b-27b4-46eb-bbec-459ea13dc189',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-21 07:28:24'),
('d760f02e-6ed8-437e-b0cb-be630c140e6c',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-21 07:28:33'),
('4907a32b-c439-4782-83fa-faf4b557b37c',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 07:28:41'),
('1598b8be-0737-4cde-a5fb-c7f4e61165e8',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 07:28:48'),
('5228c618-e0ac-4f82-91f8-d1c1ed82a6d8',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-21 07:46:22'),
('9bf51cbb-8846-4656-8968-5ea5cbdfbc37',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 07:46:33'),
('1a394c7c-c887-4234-8bfb-54f1c08e54af',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-21 07:46:48'),
('37cf97ab-d4b8-4b9c-9799-6c5b85a412a1',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-21 07:46:57'),
('3d376d4f-9f95-4107-a459-b7a95073704f',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-21 22:11:51'),
('ac3b8b0f-e436-4bf7-af93-6ba7730689d7',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-21 22:12:18'),
('07346307-85cd-4c99-989a-583819c735ec',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-21 22:13:13'),
('a00e2521-4abe-426e-8b51-c54edb8212f7',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-21 22:13:23'),
('09436810-19cb-4cfa-b1b1-da4be1f6e7e0',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-21 22:37:48'),
('bc61b668-0ecf-4e6a-ada2-165687af0ea6',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 22:38:17'),
('d7a8ded6-02d7-470b-930c-b8c42c8b4d7a',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-21 22:38:44'),
('eab96341-65ba-45de-be65-26e8e1bef041',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-22 04:53:39'),
('a344f1a8-f014-4ffc-9a30-15107f0a5a47',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'2018-12-22 04:54:42'),
('d134e15d-99b6-4597-8c55-0d13ec67d95c',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'2018-12-22 04:55:09'),
('ead78ea8-e84a-428e-8ce7-ff981fe10ce1',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-22 06:03:49'),
('7b533e13-f933-4d83-8a4c-6997810ccf2b',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-22 06:03:58'),
('1a70785d-c317-4bd3-a013-07dd8c18e302',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-22 06:08:53'),
('43a00c1d-9532-4bf8-b155-49220c2e4b99',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-22 06:09:03'),
('7ef1043c-d5c0-4adb-8fc1-29baee4aa36e',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-22 10:40:10'),
('483d5245-b254-4ab2-91a7-5695ad6ccb98',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-22 11:00:17'),
('743f6aef-6c89-482c-bdda-44079d5c849f',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-22 12:16:19'),
('4a9155c0-72f8-4626-bda1-fbc665834495',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-22 12:16:33'),
('0272dde8-9d2e-4a72-8ea3-3a0c6edc3633',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-22 17:37:25'),
('435955ba-a68b-4e62-afa7-7a879ac3e86d',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-22 17:38:01'),
('43e96c75-c807-4df1-87c3-cc2eca7e695e',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 08:53:55'),
('8365afac-ed80-4ab8-abae-93c52db3d16a',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 08:54:08'),
('a173ffd8-b4f3-49d5-bcb1-f833ed512d54',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-23 09:43:18'),
('e2dad8e6-9e1c-48cd-ac6c-63369da06fe1',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-23 09:43:25'),
('53a345bb-e047-4636-871d-6d8353c235cd',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 09:43:52'),
('c1f01c59-8545-451a-b2fe-10fa13d31bbe',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 09:44:02'),
('b19125ca-e041-41e7-8d66-1ce95e7a031c',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 11:42:08'),
('30cb26ef-8777-4ed2-80da-2ef5758bacf9',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 11:42:22'),
('cc910b73-d021-4bf9-bf16-47df988a3b6b',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 11:42:39'),
('155d3c5a-f504-4560-99d6-19bf16649a3b',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2018-12-23 11:46:09'),
('d5a088e3-aa34-44ca-92df-e10950091deb',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2018-12-23 11:47:31'),
('c692c502-3b96-4697-996c-ac982e05355d',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2018-12-23 14:02:17'),
('a5549338-acca-4656-8a87-7a2f8483d518',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 14:04:04'),
('a9f0db5a-abbc-427a-aa2e-36a1d3a93b2f',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 14:05:04'),
('0221e4b5-9fbd-4f84-90e3-c61597d9f202',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 17:07:13'),
('ef8d68f1-59a2-46f4-9272-9ccd516e04e2',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 17:07:45'),
('ef2f7234-b66f-4035-958e-de2399385069',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 17:09:51'),
('14c67595-c401-4b14-a0d4-577fe5e42afb',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 17:10:55'),
('e7791c3c-ce95-43d7-ac77-b87d86600eef',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 19:11:15'),
('fe9bee81-dcb5-4c96-9e1a-86e53472ab03',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 19:11:17'),
('00cbe153-d37b-4a03-8f9f-4176d3fa8e34',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 19:11:50'),
('5650d2fb-c04b-483d-a5a3-74e84ae0cb3e',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 19:12:00'),
('599647a2-6eb8-4553-a550-743a7ac2295b',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-23 22:31:02'),
('a3ba1a44-3c26-479e-bc28-356798be67e5',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 22:31:02'),
('39ac0bd5-b1cf-4565-a8c5-7d8921278c68',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 22:31:25'),
('de743e26-db6b-430f-801c-0a28686f62ba',	'81140209-27a9-4032-add2-642fdf8397d3',	'2018-12-23 22:31:38'),
('ec43358b-47d8-47d4-a685-0b47a76fffd3',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-26 19:27:49'),
('7f94bfd6-7f99-4d42-b165-d98438a37ab2',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-27 18:07:17'),
('3347e15d-5bc1-4967-adc9-4292bc374918',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-27 18:08:55'),
('e7b4592b-7fe1-42ac-bc35-da1c1b573f35',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2018-12-27 18:09:31'),
('daa7b90c-e181-4cab-9d7b-3f98f6a1b478',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2019-01-08 15:58:20'),
('aa6e6202-05a2-42bd-a05a-83930c74b3f1',	'632b457b-42ac-4f08-bb6a-8633aff039c2',	'2019-01-08 15:58:32'),
('b6a9760f-5e5c-43a5-9975-a9981fd3c0cd',	'7006f917-8a3d-429e-86b1-c6b37c91b504',	'2019-01-08 16:03:14'),
('a10ae67a-cca2-4ccc-9f60-d917b48bf7b4',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2019-01-11 13:48:45'),
('8d932219-9f98-490c-9eed-53e4483125c7',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2019-01-11 14:02:46'),
('07649d5b-872a-46f8-9ca8-60a510133be3',	'00c8b115-08e4-47d6-bbf5-e5967d64335b',	'2019-01-11 14:03:05'),
('d71e518f-81ce-492d-934b-1b85aa23f232',	'4cc11b45-4e0a-4362-986c-5cdb4501bbf1',	'2019-01-14 11:17:52'),
('709cdc4f-b0a1-4435-bb6e-0d9dc29d91b0',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2019-01-14 11:17:59'),
('90b3f01a-6f5d-4bed-9acc-031241067d6b',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'2019-01-14 11:36:07'),
('837d2af6-0e16-4f3f-a9ac-85d0464acc2b',	'288f7ddd-a0aa-46dc-ba3a-d91bea25ed45',	'2019-01-14 11:36:14'),
('4e783c05-7ab9-47db-bd0a-57427e26ad40',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2019-01-20 11:29:10'),
('9a41e436-7b0f-4082-8f63-03610bb763de',	'a9ab09fa-384d-472e-b3c6-3980bf77d75c',	'2019-01-20 11:29:33'),
('db62e1db-1221-4a25-9c36-994c00e35b65',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2019-01-26 13:22:24'),
('098e7861-b1e0-4153-8f99-5eca7b041616',	'b0784a65-3876-4c94-8be4-69d0275ad2a5',	'2019-01-26 13:22:53'),
('fbf312f0-102f-4bd7-a4e7-7b0527a2b39c',	'81140209-27a9-4032-add2-642fdf8397d3',	'2019-02-15 07:07:34'),
('22cb86bc-8562-4c2c-8007-357fc9b91cca',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'2019-02-16 20:51:18'),
('f4951a2a-7b29-4b55-ab54-276fe8f22daa',	'996ec78c-1371-46e0-b573-a797a41d81e0',	'2019-02-16 20:51:30');

DROP TABLE IF EXISTS `ukuran`;
CREATE TABLE `ukuran` (
  `ukuran_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ukuran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ukuran` (`ukuran_id`, `nama`) VALUES
('18941362-91d2-44cc-8e2d-0e2d14e5b989',	'S'),
('197c3a9b-aa02-4c83-9cff-86300c0c11ac',	'M'),
('ea2ff971-bd88-4cc4-b2f7-3984111011a1',	'L'),
('c8c85240-4966-4bd6-8459-afc9b17b2ce1',	'XL'),
('54c859b3-2c31-4b7a-8a6c-09f30d7d24b7',	'XXL');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Admin',	'admin',	'$2y$10$J.EbGjqFLZih/z2QwbKfb.y9y9tPQM/7e3e18b2RD9V4hVUmd8sZ6',	'YHVmmu9KODuUCxBr5bRBJaAwjDU1gIxpgDz4QuGkxL5q0iA7r6e50eUDkJI8',	'2018-12-09 06:12:11',	'2018-12-09 06:12:11'),
(2,	'User',	'user',	'$2y$10$Ib1hC1KwFie/teHREeSmyeR/gYsrvlWPB/DaaqcOTPcK4PNTN5na6',	'8eE1cJ4wo4KA1xOZaXLUA7zKwfGP1XF010kIme2R4WdOlnYKJX8qkLL7hCrB',	'2018-12-09 06:12:39',	'2018-12-09 06:12:39');

DROP TABLE IF EXISTS `warna`;
CREATE TABLE `warna` (
  `warna_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `kode` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`warna_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `warna` (`warna_id`, `nama`, `kode`) VALUES
('806c3691-57eb-4493-a849-cd5895e93640',	'Merah',	'#FF0000'),
('bfbeeb2b-9065-4576-ae8d-e25c84d8da60',	'Hitam',	'#000000'),
('9f540327-a493-46ba-ba42-73d744814838',	'Ungu',	'#BF00FF'),
('c99ed6d3-1bdc-48be-90a5-8e4d8e7696e9',	'Sea Green',	'#2E8B57'),
('e22655d8-d888-4f5b-b794-6b535c56f1bc',	'Dark Cyan',	'#008B8B'),
('1468de91-393e-40ba-b39a-f85a4281d52d',	'Teal',	'#008080');

-- 2019-02-16 14:37:57
