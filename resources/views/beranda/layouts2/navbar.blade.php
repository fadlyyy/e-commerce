<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col">
						
						<div class="main_nav_content d-flex flex-row">

							<!-- Categories Menu -->

							<div class="cat_menu_container" style="background: #008080;">
								<div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
									<div class="cat_burger"><span></span><span></span><span></span></div>
									<div class="cat_menu_text">KATEGORI</div>
								</div>

								<ul class="cat_menu">
									<?php $kategori = \App\Models\Kategori::orderBy('nama','asc')->limit(8)->get(); ?>
									@foreach($kategori as $kt)
									<li><a href="{{ url('kategori/'.$kt->kategori_id) }}">{{ $kt->nama }} <i class="fas fa-chevron-right ml-auto"></i></a></li>
									@endforeach

									@if(count(\App\Models\Kategori::orderBy('nama','asc')->get()) > 8)
									<li class="hassubs">
										<a href="#">Lainnya<i class="fas fa-chevron-right"></i></a>
										<ul>
											<?php
												$hitung = count(\App\Models\Kategori::get()) - 8;
											?>
											@foreach(\App\Models\Kategori::orderBy('nama','asc')->limit($hitung)->offset(8)->get() as $kt2)
											<li>
												<a href="#">{{ $kt2->nama }}<i class="fas fa-chevron-right"></i></a>
											</li>
											@endforeach
										</ul>
									</li>
									@endif
								</ul>
							</div>

							<!-- Main Nav Menu -->

							<div class="main_nav_menu ml-auto">
								<ul class="standard_dropdown main_nav_dropdown">
								</ul>
							</div>

							<!-- Menu Trigger -->

							<div class="menu_trigger_container ml-auto">
								<div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
									<div class="menu_burger">
										<div class="menu_trigger_text">menu</div>
										<div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</nav>