<?php
	$al = \DB::table('alamat')->first();
?>
<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="top_bar_contact_item"><div class="top_bar_icon"><img src="{{asset('onetech/images/phone.png')}}" alt=""></div>{{ $al->nope }}</div>
						<div class="top_bar_contact_item"><div class="top_bar_icon"><img src="{{asset('onetech/images/mail.png')}}" alt=""></div><a href="mailto:fastsales@gmail.com">{{ $al->email }}</a></div>
						<div class="top_bar_content ml-auto">
							
							<div class="top_bar_user">
								<div class="user_icon"><img src="{{asset('onetech/images/user.svg')}}" alt=""></div>
								<div><a href="{{ url('register') }}">Register</a></div>
								<div><a href="{{ url('login') }}">Sign in</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>