@extends('beranda.layouts2.master')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('onetech/styles/bootstrap4/bootstrap.min.css') }}">
<link href="{{ asset('onetech/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('onetech/styles/cart_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('onetech/styles/cart_responsive.css') }}">

<div class="cart_section">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<div class="cart_container">
					<div class="cart_title">Shopping Cart</div>
					
					<div class="cart_items">
					    
					    @foreach(\Cart::content() as $ct)
						<ul class="cart_list">
							<li class="cart_item clearfix">
							    <?php
							        $gambar = \App\Models\Product::where('product_id',$ct->id)->first();
							    ?>
								<div class="cart_item_image"><img src="{{ asset('uploads/'.$gambar->gambar->nama) }}" alt=""></div>
								<div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
									<div class="cart_item_name cart_info_col">
										<div class="cart_item_title">Name</div>
										<div class="cart_item_text">{{ $ct->name }}</div>
									</div>
									<div class="cart_item_color cart_info_col">
										<div class="cart_item_title">Warna</div>
										<div class="cart_item_text"><span style="background-color:{{ \App\Models\Warna::where('warna_id',$ct->options->warna_id)->value('kode') }};"></span>{{ \App\Models\Warna::where('warna_id',$ct->options->warna_id)->value('nama') }}</div>
									</div>
									<div class="cart_item_color cart_info_col">
										<div class="cart_item_title">Warna</div>
										<div class="cart_item_text">{{ \App\Models\Ukuran::where('ukuran_id',$ct->options->ukuran_id)->value('nama') }}</div>
									</div>
									<div class="cart_item_quantity cart_info_col">
										<div class="cart_item_title">Quantity</div>
										<div class="cart_item_text">{{ $ct->qty }}</div>
									</div>
									<div class="cart_item_price cart_info_col">
										<div class="cart_item_title">Price</div>
										<div class="cart_item_text">Rp. {{ number_format($ct->price,0) }}</div>
									</div>
									<div class="cart_item_total cart_info_col">
										<div class="cart_item_title">Total</div>
										<div class="cart_item_text">Rp. {{ number_format($ct->price * $ct->qty,0) }}</div>
									</div>
								</div>
							</li>
						</ul>
						@endforeach
						
					</div>
					
					<!-- Order Total -->
					<div class="order_total">
						<div class="order_total_content text-md-right">
							<div class="order_total_title">Order Total:</div>
							<div class="order_total_amount">Rp. {{ str_replace(',','.',\Cart::subtotal()) }}</div>
						</div>
					</div>

					<div class="cart_buttons">
						<button type="button" class="button cart_button_clear"><a href="{{ url('keranjang/remove') }}" style="color:black;">Kosongkan Keranjang</a></button>
						<button type="button" class="button cart_button_checkout"><a href="{{ url('ongkir/index') }}">Lanjut Bayar</a></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{{asset('onetech/js/jquery-3.3.1.min.js')}}"></script>

<script type="text/javascript">
		$(document).ready(function(){
		    var flash = "{{ Session::has('pesan') }}";
		    if(flash){
		        var pesan = "{{ Session::get('pesan') }}";
		        alert(pesan);
		    }
		    
			$('.btn-cart').click(function(e){
				e.preventDefault();
				$('#modal-cart').modal();
			});
		});
	</script>

@endsection