@extends('beranda.layouts2.master')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('onetech/styles/bootstrap4/bootstrap.min.css') }}">
<link href="{{ asset('onetech/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('onetech/styles/cart_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('onetech/styles/cart_responsive.css') }}">

<div class="cart_section">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<div class="cart_container">
					<div class="cart_title">Periksa Ongkir</div>
					
					<div class="cart_items">
					    
					    
						
					</div>
					
					<!-- Order Total -->
				<form action="{{ url('bayar') }}" method="get">
					<div class="row">
					    <div class="col-md-6">
					        
					          <div class="form-group">
                                <label for="exampleInputEmail1">Nama Pengirim</label>
                                <input type="text" name="nama1" value="{{ old('nama1') }}" class="form-control">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Provinsi Asal</label>
                                <select class="provinsi1 form-control" name="provinsi1">
                                    <option selected="" disabled="">Pilih Provinsi</option>
                                    @foreach($provinsi->rajaongkir->results as $ps)
                                    <option value="{{ $ps->province_id }}">{{ $ps->province }}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Kota Asal</label>
                                <select class="kota1 form-control" name="kota1">
                                    <option selected="" disabled="">Pilih Kota</option>
                                    
                                </select>
                              </div>
                            
					    </div>
					    <div class="col-md-6">
					        
					          <div class="form-group">
                                <label for="exampleInputEmail1">Nama Penerima</label>
                                <input type="text" name="nama2" value="{{ old('nama2') }}" class="form-control">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Provinsi Tujuan</label>
                                <select class="provinsi2 form-control" name="provinsi2">
                                    <option selected="" disabled="">Pilih Provinsi</option>
                                    @foreach($provinsi->rajaongkir->results as $ps)
                                    <option value="{{ $ps->province_id }}">{{ $ps->province }}</option>
                                    @endforeach
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Kota Tujuan</label>
                                <select class="kota2 form-control" name="kota2">
                                    <option selected="" disabled="">Pilih Kota</option>
                                    
                                </select>
                              </div>
                            
					    </div>
					    
					</div>
					
					<div class="row">
					    <div class="col-md-6">
					        
                              <div class="form-group">
                                <label for="exampleInputEmail1">Kurir</label>
                                <select class="kurir form-control" name="kurir">
                                    <option selected="" disabled="">Pilih Kurir</option>
                                    <option value="jne">JNE</option>
                                    <option value="tiki">TIKI</option>
                                    <option value="pos">POS</option>
                                </select>
                              </div>
                            
					    </div>
					    <div class="col-md-6">
					        
                              <div class="form-group">
                                <label for="exampleInputEmail1">Kode Pos</label>
                                <input type="number" class="form-control" name="kode_pos" value="{{ old('kode_pos') }}">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">No. Handphone</label>
                                <input type="number" class="form-control" name="nope" value="{{ old('nope') }}">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Alamat Lengkap</label>
                                <textarea class="form-control" name="alamat" rows="10">Misal: Kp.contoh, jl. Lamin rt02/03 no.04 kel.jatiluhur kec.jatiasih Bekasi Jawa barat 17425</textarea>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Layanan</label>
                                <select class="form-control layananNya" name="layanan">
                                    <option selected="" disabled="">Klik Cek Ongkir terlebih Dahulu</option>
                                </select>
                              </div>
                            
					    </div>
					</div>

					<div class="cart_buttons">
						<button type="button" class="button cart_button_clear"><a href="{{ url('keranjang/remove') }}" class="cek-ongkir" style="color:black;">Cek Ongkir</a></button>
						<button style="display:none;" type="submit" class="button cart_button_checkout">Lanjut Bayar</button>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-ongkir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--<h4 class="modal-title" id="myModalLabel">Modal title</h4>-->
      </div>
      <div class="modal-body">
        <p><b><i>Harap tunggu.. </i></b></p>
      </div>
    </div>
  </div>
</div>

<script src="{{asset('onetech/js/jquery-3.3.1.min.js')}}"></script>

<script type="text/javascript">
		$(document).ready(function(){
		    var flash = "{{ Session::has('pesan') }}";
		    if(flash){
		        var pesan = "{{ Session::get('pesan') }}";
		        alert(pesan);
		    }

		    var masalah = "{{ $errors->any() }}";
		    if(masalah){
		    	alert('Semua form wajib diisi');
		    }

		    var provinsi1 = '';
		    var provinsi2 = '';
		    var kota1 = '';
		    var kota2 = '';
		    
		    // Cek Ongkir
		    $('.cek-ongkir').click(function(e){
		        e.preventDefault();
		        $('.hasil-ongkir').empty();
		        $('#modal-ongkir').modal();
		        $("select[name='layanan']").empty();
		        
		        var kota_asal = $('.kota1').val();
		        var kota_tujuan = $('.kota2').val();
		        var kurir = $('.kurir').val();
		        var berat = "{{ $berat }}";
		        
		        $.ajax({
		            type:'get',
		            url:"{{ url('ongkir/cek') }}",
		            data:{
		                kota_asal: kota_asal,
		                kota_tujuan: kota_tujuan,
		                kurir: kurir,
		                berat: berat,
		            },
		            success: function(data){
		                $.each(data.hasil.rajaongkir.results,function(i,v){

                        // var hasil = '<table class="table table-bordered">';
                        // var hasil = '<tbody>';
                        
                            $.each(v.costs,function(i,v){
                                console.log(v);
    
                                var layanan = v.service;
                                var cost = 0;
                                
                                $.each(v.cost,function(i,v){
                                    cost = v.value;
                                })
                                
                                console.log(layanan+'-'+cost);
                                
                                var hasil = '';
                                
                                hasil += '<option value="'+layanan+'-'+cost+'">';
                                hasil += layanan+'-'+cost;
                                hasil += '</option>';
                                
                                $('.layananNya').append(hasil);
                                
                            });
    
                            // hasil+='</tbody>'
                            // hasil+= '</table>';
    
                            
                        })
                        $('.cart_button_checkout').show();
                        $('#modal-ongkir').modal('hide');
		            }
		        })
		    })
		    
			$('.btn-cart').click(function(e){
				e.preventDefault();
				$('#modal-cart').modal();
			});
			
			$('body').on('change','.provinsi1',function(e){
			    e.preventDefault();
			    var id = $(this).val();
			    $('.kota1').empty();
			    $.ajax({
			        type:'get',
			        dataType:'json',
			        url:"{{ url('ongkir/kota') }}"+'/'+id,
			        success:function(data){
			            console.log(data.data.rajaongkir.results);
			            
			            var hasil = '';
			            $.each(data.data.rajaongkir.results,function(i,v){
			                hasil += '<option value="'+v.city_id+'">';
			                
			                hasil += v.city_name
			                
			                hasil += '</option>';
			            })
			            
			            $('.kota1').append(hasil)
			        }
			    })
			})
			
			$('body').on('change','.provinsi2',function(e){
			    e.preventDefault();
			    var id = $(this).val();
			    $('.kota2').empty();
			    $.ajax({
			        type:'get',
			        dataType:'json',
			        url:"{{ url('ongkir/kota') }}"+'/'+id,
			        success:function(data){
			            console.log(data.data.rajaongkir.results);
			            
			            var hasil = '';
			            $.each(data.data.rajaongkir.results,function(i,v){
			                hasil += '<option value="'+v.city_id+'">';
			                
			                hasil += v.city_name
			                
			                hasil += '</option>';
			            })
			            
			            $('.kota2').append(hasil)
			            
			            
			        }
			    })
			})
		});
	</script>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

@endsection