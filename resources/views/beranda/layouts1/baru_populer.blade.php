<div class="new_arrivals">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="tabbed_container">
						<div class="tabs clearfix tabs-right">
							<div class="new_arrivals_title">List produk</div>
							<ul class="clearfix">
								<li class="active">Featured</li>
								<li></li>
								<li></li>
							</ul>
							<div class="tabs_line"><span></span></div>
						</div>
						<div class="row">
							<div class="col-lg-9" style="z-index:1;">

								<!-- Product Panel -->
								<div class="product_panel panel active">
									<div class="arrivals_slider slider">

										<!-- Slider Item -->
										<?php $produk = \App\Models\Product::where('status',1)->orderBy('created_at','desc')->limit(10)->get(); ?>

										@foreach($produk as $pr)
										<div class="arrivals_slider_item" style="width: 300px;">
											<div class="border_active"></div>
											<div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
												<div class="product_image d-flex flex-column align-items-center justify-content-center">
													<a href="{{ url('detail/'.$pr->product_id) }}">
														<img style="width: 115px;height: 81px;" src="{{asset('uploads/'.$pr->gambar->nama)}}" alt="">
													</a>
												</div>
												<div class="product_content">
													<a style="color: #008080" href="{{ url('detail/'.$pr->product_id) }}">
														<div class="product_price">
															Rp. {{ str_replace(',','.',number_format($pr->harga_akhir,0)) }}
														</div>
													</a>
													<div class="product_name">
														<div>
															<a href="{{ url('detail/'.$pr->product_id) }}">
																<p style="word-wrap: break-word;">
																	{{ $pr->nama }}
																</p>
															</a>
														</div>
													</div>
													<div class="product_extras">
														<div class="product_color">
															
														</div>
														
													</div>
												</div>
												<ul class="product_marks">
													<li class="product_mark product_discount">-25%</li>
													@if($pr->discount > 0)
													<li style="background: red;" class="product_mark product_new">-{{ $pr->discount }}%</li>
													@endif
												</ul>
											</div>
										</div>
										@endforeach

									</div>
									<div class="arrivals_slider_dots_cover"></div>
								</div>

								<!-- Product Panel -->
								

							</div>

							<?php $pk = \App\Models\Product::where('status',1)->orderBy('created_at','desc')->first(); ?>
							<div class="col-lg-3">
								<div class="arrivals_single clearfix">
									<div class="d-flex flex-column align-items-center justify-content-center">
										<div class="arrivals_single_image">
											<a href="{{ url('detail/'.$pk->product_id) }}">
												<img style="width: 212px;height: 200px;" src="{{asset('uploads/'.$pk->gambar->nama)}}" alt="">
											</a>
										</div>
										<div class="arrivals_single_content">
											<div class="arrivals_single_category"><a href="{{ url('kategori/'.$pk->kategori->kategori_id) }}">{{ $pk->kategori->nama }}</a></div>
											<div class="arrivals_single_name_container clearfix">
												<div class="arrivals_single_name"><a style="color: black;" href="{{ url('detail/'.$pk->product_id) }}">{{ $pk->nama }}</a></div>
												<div style="color: #008080;" class="arrivals_single_price text-right">Rp. {{ str_replace(',','.',number_format($pk->harga_akhir,0)) }}</div>
											</div>
											<form action="#"><button class="arrivals_single_button">Produk Lainnya</button></form>
										</div>
										<ul class="arrivals_single_marks product_marks">
											<li style="background: red;" class="arrivals_single_mark product_mark product_new">new</li>
										</ul>
									</div>
								</div>
							</div>

						</div>
								
					</div>
				</div>
			</div>
		</div>		
	</div>

