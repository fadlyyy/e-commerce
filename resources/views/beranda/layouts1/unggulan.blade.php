<div class="featured">
						<div class="tabbed_container">
							<div class="tabs">
								<ul class="clearfix">
									<li class="active">Unggulan</li>
									<!-- <li>On Sale</li> -->
									<!-- <li>Best Rated</li> -->
								</ul>
								<div class="tabs_line"><span></span></div>
							</div>

							<!-- Product Panel -->
							<div class="product_panel panel active">
								<div class="featured_slider slider">

									<!-- Slider Item -->
									@foreach($unggulan as $ug)
									<div class="featured_slider_item">
										<div class="border_active"></div>
										<div class="product_item discount d-flex flex-column align-items-center justify-content-center text-center">
											<div class="product_image d-flex flex-column align-items-center justify-content-center">
												<a href="{{ url('detail/'.$ug->produk->product_id) }}">
													<img style="width: 115px;height: 115px;" src="{{asset('uploads/'.$ug->produk->gambar->nama)}}" alt="">
												</a>
											</div>
											<div class="product_content">
												<div class="product_price discount">
													<span style="color: #008080;">Rp. {{ str_replace(',','.',number_format($ug->produk->harga_akhir,0)) }}</span>

													@if($ug->produk->discount > 0)
													<span>
														Rp. {{ str_replace(',','.',number_format($ug->produk->harga_awal,0)) }}
													</span>
													@endif

												</div>
												<div class="product_name"><div><a href="{{ url('detail/'.$ug->produk->product_id) }}">{{ str_limit($ug->produk->nama,15) }}</a></div></div>
												<div class="product_extras">
													<div class="product_color">
														@foreach($ug->produk->warnas as $wn)
														<input type="radio" checked name="product_color" style="background:{{ $wn->warna->kode }}">
														@endforeach
													</div>
												</div>
											</div>
											<ul class="product_marks">
												@if($ug->produk->discount > 0)
												<li class="product_mark product_discount">-{{ $ug->produk->discount }}%</li>
												@endif
												<li class="product_mark product_new">new</li>
											</ul>
										</div>
									</div>
									@endforeach

								</div>
								<div class="featured_slider_dots_cover"></div>
							</div>

							

						</div>
					</div>