@extends('beranda.layouts2.master')

@section('content')



<div class="banner_2">
		<div class="banner_2_background" style="background-image:url({{asset('onetech/images/banner_2_background.jpg')}})"></div>
		<div class="banner_2_container">
			<div class="banner_2_dots"></div>
			<!-- Banner 2 Slider -->

			<div class="owl-carousel owl-theme banner_2_slider">

				<!-- Banner 2 Slider Item -->
				<div class="owl-item">
					<div class="banner_2_item">
						<div class="container fill_height">
							<div class="row fill_height">
								<div class="col-lg-4 col-md-6 fill_height">
									<div class="banner_2_content">
										<div class="banner_2_category">Laptops</div>
										<div class="banner_2_title">MacBook Air 13</div>
										<div class="banner_2_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</div>
										<div class="rating_r rating_r_4 banner_2_rating"><i></i><i></i><i></i><i></i><i></i></div>
										<div class="button banner_2_button"><a href="#">Explore</a></div>
									</div>
									
								</div>
								<div class="col-lg-8 col-md-6 fill_height">
									<div class="banner_2_image_container">
										<div class="banner_2_image"><img src="{{asset('onetech/images/banner_2_product.png')}}" alt=""></div>
									</div>
								</div>
							</div>
						</div>			
					</div>
				</div>

				<!-- Banner 2 Slider Item -->
				<div class="owl-item">
					<div class="banner_2_item">
						<div class="container fill_height">
							<div class="row fill_height">
								<div class="col-lg-4 col-md-6 fill_height">
									<div class="banner_2_content">
										<div class="banner_2_category">Laptops</div>
										<div class="banner_2_title">MacBook Air 13</div>
										<div class="banner_2_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</div>
										<div class="rating_r rating_r_4 banner_2_rating"><i></i><i></i><i></i><i></i><i></i></div>
										<div class="button banner_2_button"><a href="#">Explore</a></div>
									</div>
									
								</div>
								<div class="col-lg-8 col-md-6 fill_height">
									<div class="banner_2_image_container">
										<div class="banner_2_image"><img src="images/banner_2_product.png" alt=""></div>
									</div>
								</div>
							</div>
						</div>			
					</div>
				</div>

				<!-- Banner 2 Slider Item -->
				<div class="owl-item">
					<div class="banner_2_item">
						<div class="container fill_height">
							<div class="row fill_height">
								<div class="col-lg-4 col-md-6 fill_height">
									<div class="banner_2_content">
										<div class="banner_2_category">Laptops</div>
										<div class="banner_2_title">MacBook Air 13</div>
										<div class="banner_2_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas fermentum laoreet.</div>
										<div class="rating_r rating_r_4 banner_2_rating"><i></i><i></i><i></i><i></i><i></i></div>
										<div class="button banner_2_button"><a href="#">Explore</a></div>
									</div>
									
								</div>
								<div class="col-lg-8 col-md-6 fill_height">
									<div class="banner_2_image_container">
										<div class="banner_2_image"><img src="images/banner_2_product.png" alt=""></div>
									</div>
								</div>
							</div>
						</div>			
					</div>
				</div>

			</div>
		</div>
	</div>

<div class="shop">
		<div class="container">

			

			<div class="row">

				<div class="col-lg-10">


					
					<!-- Shop Content -->

					<div class="shop_content">
						<div class="shop_bar clearfix">
							<div class="shop_product_count"><span>{{ $hitung }}</span> products found</div>
							
						</div>

						<div class="product_grid">
							<div class="product_grid_border"></div>

							<!-- Product Item -->
							@foreach($produk as $pk)
							<a href="{{ url('detail/'.$pk->product_id) }}">
								<div class="product_item discount">
									<div class="product_border"></div>
									<div class="product_image d-flex flex-column align-items-center justify-content-center"><img style="width: 115px;height: 115px;" src="{{ asset('uploads/'.$pk->gambar->nama) }}" alt=""></div>
									<div class="product_content">
										<div class="product_price">Rp. {{ str_replace(',','.',number_format($pk->harga_akhir,0)) }}</div>
										<div class="product_name"><div>{{ str_limit($pk->nama,25) }}</a></div></div>
									</div>
									<ul class="product_marks">
										@if($pk->discount > 0)
										<li class="product_mark product_discount">-{{ $pk->discount }}%</li>
										@endif
									</ul>
								</div>
							</a>
							@endforeach

						</div>

						<!-- Shop Page Navigation -->

						<div class="shop_page_nav d-flex flex-row">
							{!! $produk->links() !!}
						</div>

					</div>

				</div>

				<div class="col-lg-2">

					<!-- Shop Sidebar -->
					<div class="clearfix">
						
						<div class="sidebar_section filter_by_section">
							<div class="sidebar_title">Filter By</div>
						</div>
						<div class="sidebar_section">
							<div class="sidebar_subtitle brands_subtitle">Brands</div>
							<ul class="brands_list">
								<li class="brand"><a href="#">Apple</a></li>
								<li class="brand"><a href="#">Beoplay</a></li>
								<li class="brand"><a href="#">Google</a></li>
								<li class="brand"><a href="#">Meizu</a></li>
								<li class="brand"><a href="#">OnePlus</a></li>
								<li class="brand"><a href="#">Samsung</a></li>
								<li class="brand"><a href="#">Sony</a></li>
								<li class="brand"><a href="#">Xiaomi</a></li>
							</ul>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<script src="{{asset('onetech/js/jquery-3.3.1.min.js')}}"></script>
	<script src="{{asset('onetech/styles/bootstrap4/popper.js')}}"></script>
	<script src="{{asset('onetech/styles/bootstrap4/bootstrap.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/TweenMax.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/TimelineMax.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/animation.gsap.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
	<script src="{{asset('onetech/plugins/easing/easing.js')}}"></script>
	<script src="{{asset('onetech/js/product_custom.js')}}"></script>

@endsection