@extends('beranda.layouts2.master')

@section('content')

<title>Single Product</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="OneTech shop project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{asset('onetech/styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('onetech/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{asset('onetech/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('onetech/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('onetech/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('onetech/styles/product_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('onetech/styles/product_responsive.css')}}">

<style type="text/css">
	tbody {
		border-top: 2px solid black;
	}
</style>

<div class="single_product">
		<div class="container">
			<div class="row">

				<!-- Images -->
				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						@foreach($data->gambars as $dt)
						<li data-image="{{asset('uploads/'.$dt->nama)}}"><img src="{{asset('uploads/'.$dt->nama)}}" alt=""></li>
						@endforeach
					</ul>
				</div>

				<!-- Selected Image -->
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="image_selected"><img src="{{asset('uploads/'.$data->gambar->nama)}}" alt=""></div>
				</div>

				<!-- Description -->
				<div class="col-lg-5 order-3">
					<div class="product_description">
						<div class="product_category">{{ $data->kategori->nama }}</div>
						<div class="product_name">{{ $data->nama }}</div>
						<!-- <div class="rating_r rating_r_4 product_rating"><i></i><i></i><i></i><i></i><i></i></div> -->
						<table class="table">
							<tbody>
								<tr>
									<th>Nama</th>
									<th>:</th>
									<td>{{ $data->nama }}</td>
								</tr>
								<tr>
									<th>Kategori</th>
									<th>:</th>
									<td>{{ $data->kategori->nama }}</td>
								</tr>
								@if($data->discount > 0)
								<tr>
									<th>Discount</th>
									<th>:</th>
									<td>{{ $data->discount }}%</td>
								</tr>
								@endif
								<tr>
									<th>Harga</th>
									<th>:</th>
									<td>Rp. {{ str_replace(',','.',number_format($data->harga_akhir,0)) }}</td>
								</tr>
								<tr>
									<th>Stock</th>
									<th>:</th>
									<td>{{ $data->stock }}</td>
								</tr>
								<tr>
									<th>Ukuran</th>
									<th>:</th>
									<td>
										@foreach($data->ukurans as $wn)
										{{ $wn->ukuran->nama }} |
										@endforeach
									</td>
								</tr>
								<tr>
									<th>Warna</th>
									<th>:</th>
									<td>
										@foreach($data->warnas as $wn)
										{{ $wn->warna->nama }} |
										@endforeach
									</td>
								</tr>
								<tr>
									<th>Berat</th>
									<th>:</th>
									<td>{{ $data->berat }} gram</td>
								</tr>
								<tr>
									<th colspan="3"><button style="background: #008080;" type="button" class="button cart_button btn-cart">Masukkan ke Keranjang</button></th>
								</tr>
							</tbody>
						</table>

						
						
					</div>
				</div>

			</div>

			<div class="row">
				<div class="col-lg-2 order-lg-1 order-2">
				</div>
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="order_info d-flex flex-row">
							<form action="#">
								
								<div class="product_text"><p>{!! $data->keterangan !!}</p></div>
								
							</form>
						</div>
				</div>

				<div class="col-lg-5 order-3">
					<div class="product_description">
						
					</div>
				</div>

			</div>

		</div>
	</div>

	<!-- Modal -->
<div class="modal fade" id="modal-cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        
      		<form method="get" action="{{ url('add-to-cart/'.$data->product_id) }}">
			  <div class="form-group">
			    <label for="exampleInputEmail1">Pilih Warna</label>
			    <table class="table">
			    	@foreach($data->warnas->chunk(3) as $wnChunk)
			    		<tr>
			    			@foreach($wnChunk as $wn)
			    				<td><input type="radio" name="warna_id" value="{{ $wn->warna->warna_id }}"> {{ $wn->warna->nama }}</td>
			    			@endforeach
			    		</tr>
			    	@endforeach
			    </table>
			  </div>


			  <div class="form-group">
			    <label for="exampleInputPassword1">Ukuran</label>
			    <table class="table">
			    	@foreach($data->ukurans->chunk(3) as $wnChunk)
			    		<tr>
			    			@foreach($wnChunk as $wn)
			    				<td><input type="radio" name="ukuran_id" value="{{ $wn->ukuran->ukuran_id }}"> {{ $wn->ukuran->nama }}</td>
			    			@endforeach
			    		</tr>
			    	@endforeach
			    </table>
			  </div>
			  
			  
			  <div class="form-group">
			    <label for="exampleInputEmail1">Qty</label>
			    <input type="number" class="form-control" name="qty">
			  </div>
			  
			  
			  <button style="background: #09d261;cursor:pointer;" type="submit" class="btn btn-default">Submit</button>
			</form>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

	<script src="{{asset('onetech/js/jquery-3.3.1.min.js')}}"></script>
	<script src="{{asset('onetech/styles/bootstrap4/popper.js')}}"></script>
	<script src="{{asset('onetech/styles/bootstrap4/bootstrap.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/TweenMax.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/TimelineMax.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/animation.gsap.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
	<script src="{{asset('onetech/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
	<script src="{{asset('onetech/plugins/easing/easing.js')}}"></script>
	<script src="{{asset('onetech/js/product_custom.js')}}"></script>

	<script type="text/javascript">
		$(document).ready(function(){
		    var flash = "{{ Session::has('pesan') }}";
		    if(flash){
		        var pesan = "{{ Session::get('pesan') }}";
		        alert(pesan);
		    }
		    
			$('.btn-cart').click(function(e){
				e.preventDefault();
				$('#modal-cart').modal();
			});
		});
	</script>

@endsection