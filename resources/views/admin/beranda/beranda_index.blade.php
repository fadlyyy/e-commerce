@extends('admin.layouts.master')

@section('content')

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="box">
			<div class="box-body">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Carousel</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
								<li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
							</ol>
							<div class="carousel-inner">
								<div class="item active">
									<center>
										<img style="width: 50%;" src="{{ asset('hitam.png') }}" alt="First slide">
									</center>

									<div class="carousel-caption">
										First Slide
									</div>
								</div>
								<div class="item">
									<center>
										<img style="width: 50%;" src="{{ asset('NAVY.png') }}" alt="Second slide">
									</center>

									<div class="carousel-caption">
										Second Slide
									</div>
								</div>
								<div class="item">
									<center>
										<img style="width: 50%;" src="{{ asset('LIGHTSLATEGRAY.png') }}" alt="Third slide">
									</center>

									<div class="carousel-caption">
										Third Slide
									</div>
								</div>
							</div>
							<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
								<span class="fa fa-angle-left"></span>
							</a>
							<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
								<span class="fa fa-angle-right"></span>
							</a>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="box">
		<div class="box-body">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<!-- <li class="active"><a href="#tab_1" data-toggle="tab">Tab 1</a></li>
					<li><a href="#tab_2" data-toggle="tab">Tab 2</a></li>
					<li><a href="#tab_3" data-toggle="tab">Tab 3</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							Dropdown <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
							<li role="presentation" class="divider"></li>
							<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
						</ul>
					</li>
					<li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> -->
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<center>
							<h3><b><i><a target="_blank" href="https://shopee.co.id/Celana-Sirwal-Pangsi-jogger-Al-Hanif-Murah-Berkualitas-i.40578082.632356908">Belanja Disini</a></i></b></h3>
						</center>
						<b>Deskripsi:</b>

						<p>
							Kondisi : Baru <br>
							Ukuran : All Size Dewasa (Lingkar pinggang sampai dengan +- 105cm), panjang 85cm, lingkar paha +- 65cm
							<br>
							<br>
							..	
							Warna :
							- hitam <br>
							- Navy <br>
							- Alice Blue / putih kebiruan <br>
							- Biru Muda <br>
							- Cream <br>
							- Putih Susu <br>
							..
							- Bahan Katun Twill, tebal lembut dan dingin <br>
							- Pinggang Stretch (Karet), terdapat 6 buah kantong <br>
							..
							<br>
							Manfaat : <br>
							- Nyaman <br>
							- Adem dipakai <br>
							- Cocok untuk santai dan terlihat casual saat bepergian <br>
							<br>
							<center>
								<h3><b><i><a target="_blank" href="https://shopee.co.id/Celana-Sirwal-Pangsi-jogger-Al-Hanif-Murah-Berkualitas-i.40578082.632356908">Belanja Disini</a></i></b></h3>
							</center>
						</p>
						<p><b><i>**Silahkan dibeli yahh.. agar mimin lbih semangat lagi buat ngasih source code gratis :)**</i></b></p>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')



@endsection