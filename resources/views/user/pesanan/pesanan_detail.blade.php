@extends('user.layouts.master')

@section('content')


<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="box">
			
			<div class="box-body">

				<h2>Alamat</h2>
				<table class="table">
					<tbody>
						<tr>
							<th>Nama</th>
							<td>:</td>
							<td>{{ $alamat->nama2 }}</td>
						</tr>
						<tr>
							<th>No. Hp</th>
							<td>:</td>
							<td>{{ $alamat->nope }}</td>
						</tr>
						<tr>
							<th>Kode Pos</th>
							<td>:</td>
							<td>{{ $alamat->kode_pos }}</td>
						</tr>
						<tr>
							<th>Alamat</th>
							<td>:</td>
							<td>{{ $alamat->alamat }}</td>
						</tr>
						<tr>
							<th>Layanan</th>
							<td>:</td>
							<td>{{ $alamat->kurir }} {{ $alamat->layanan }}</td>
						</tr>
						<tr>
							<th>Ongkir</th>
							<td>:</td>
							<td>Rp. {{ $alamat->ongkir }}</td>
						</tr>
					</tbody>
				</table>

			</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			
			<div class="box-body">

				<h2>List Barang</h2>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Gambar</th>
							<th>Nama</th>
							<th>Qty</th>
							<th>Harga</th>
							<th>SubHarga</th>
							<th>Warna</th>
							<th>Ukuran</th>
							<th>Berat</th>
						</tr>
					</thead>
					<tbody>
						@foreach($barangs as $index=>$br)
						<tr>
							<td>{{ $index+1 }}</td>
							<td><img style="width: 50px;" src="{{ asset('uploads/'.$br->produk->gambar->nama) }}"></td>
							<td>{{ $br->produk->nama }}</td>
							<td>{{ $br->qty }}</td>
							<td>Rp. {{ str_replace(',','.',number_format($br->harga,0)) }}</td>
							<td>Rp. {{ str_replace(',','.',number_format($br->subharga,0)) }}</td>
							<td>{{ $br->warna->nama }}</td>
							<td>{{ $br->ukuran->nama }}</td>
							<td>{{ $br->berat*$br->qty }} Gram</td>
						</tr>
						@endforeach
					</tbody>
					<tbody>
						
					</tbody>
				</table>

			</div>

		</div>
	</div>
</div>


@endsection

@section('scripts')



@endsection