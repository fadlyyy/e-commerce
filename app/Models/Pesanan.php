<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $table = 'pesanan';
    public $primaryKey = 'pesanan_id';
    public $timestamps = false;
    protected $casts = ['pesanan_id'=>'string'];

    public function alamat(){
    	return $this->belongsTo('App\Models\Pesanan_alamat','pesanan_id','pesanan_id');
    }

    public function barangs(){
    	return $this->hasMany('App\Models\Pesanan_barang','pesanan_id','pesanan_id');
    }

    public function statuss(){
        return $this->belongsTo('App\Models\Pesanan_status','status','status');
    }
}
