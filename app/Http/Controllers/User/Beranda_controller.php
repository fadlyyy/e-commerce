<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Beranda_controller extends Controller
{
    public function index(){
    	$title = 'Sangcahaya.com | Beranda User';

    	return view('user.beranda.beranda_index',compact('title'));
    }
}
