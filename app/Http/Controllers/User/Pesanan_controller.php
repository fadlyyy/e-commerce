<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\Models\Pesanan;
use App\Models\Pesanan_alamat;
use App\Models\Pesanan_barang;

class Pesanan_controller extends Controller
{
    public function index(){
    	$title = 'Sangcahaya.com | Semua Pesanan';
    	$data = Pesanan::where('user_id',\Auth::user()->id)->orderBy('tanggal','desc')->get();

    	return view('user.pesanan.pesanan_index',compact('title','data'));
    }

    public function detail($id){
    	$title = 'Sangcahaya.com | Detail Pesanan';
    	$alamat = Pesanan_alamat::where('pesanan_id',$id)->first();
    	$barangs = Pesanan_barang::where('pesanan_id',$id)->get();

    	return view('user.pesanan.pesanan_detail',compact('title','alamat','barangs'));
    }

    public function konfirmasi(){
    	$title = 'Sangcahaya.com | Konfirmasi Pesanan';
    	$data = Pesanan::where('user_id',\Auth::user()->id)->where('status',2)->orderBy('tanggal','desc')->get();

    	return view('user.pesanan.pesanan_konfirmasi',compact('title','data'));
    }

    public function konfirmasi_detail($id){
    	$title = 'Sangcahaya.com | Konfirmasi Pesanan';
    	$data = Pesanan::orderBy('tanggal','desc')->get();
    	$pesanan_id = $id;

    	return view('user.pesanan.pesanan_konfirmasi_detail',compact('title','data','pesanan_id'));
    }

    public function konfirmasi_proses(Request $request, $id){
    	$file = $request->file('image');

    	if($file){
    		$nama_gambar = $file->getClientOriginalName();
    		\Image::make(Input::file('image'))->resize(263, 280)->save('uploads/'.$nama_gambar);

    		\DB::transaction(function() use($id,$nama_gambar){
                \DB::table('pesanan_konfirmasi')->insert([
                    'pesanan_konfirmasi_id'=>\Uuid::generate(4),
                    'pesanan_id'=>$id,
                    'gambar'=>$nama_gambar
                ]);

                \DB::table('pesanan')->where('pesanan_id',$id)->update([
                    'status'=>1,
                ]);
            });

    		\Session::flash('pesan','Bukti Pembayaran berhasil dikirim');
    		return redirect('user/pesanan/konfirmasi');
    	}else{
    		\Session::flash('pesan','Gambar wajib dimasukkan');
    		return redirect()->back();
    	}
    }
}
