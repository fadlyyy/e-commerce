<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Kategori;
use App\Models\Recent_view;

class Beranda_controller extends Controller
{
    public function index(){
    	$title = 'Sangcahaya.com | Shop.com';
    	$banner = \App\Models\Banner::first();
    	$populer_minggu = \App\Models\Populer_minggu::orderBy('urutan','asc')->get();
    	
        
    	$unggulan = \App\Models\Featured::orderBy('urutan','asc')->get();
    	$slider = \App\Models\Banner_slider::orderBy('urutan','asc')->get();
    	$best_seller = \App\Models\Best_seller::get();
        $produk = \App\Models\Product::where('status',1)->orderBy('created_at','desc')->paginate(15);
        $hitung = count(\App\Models\Product::where('status',1)->get());

    	return view('beranda.layouts1.master',compact('title','banner','populer_minggu','kategori','unggulan','slider','best_seller','produk','kategori_real','hitung'));
    }

    public function detail($id){
        $title = Product::where('product_id',$id)->value('nama');
        $data = Product::where('product_id',$id)->first();
        $hitung = count(\App\Models\Product::where('status',1)->where('kategori_id',$id)->get());

        Recent_view::insert([
            'recent_view_id'=>\Uuid::generate(4),
            'product_id'=>$id,
            'created_at'=>date("Y-m-d H:i:s"),
        ]);

        return view('beranda.detail',compact('title','data','hitung'));
    }

    public function kategori($id){
        $title = Kategori::where('kategori_id',$id)->value('nama');
        $produk = Product::where('kategori_id',$id)->where('status',1)->paginate(15);
        $hitung = count(\App\Models\Product::where('status',1)->where('kategori_id',$id)->get());

        return view('beranda.home',compact('title','produk','hitung'));
    }

    public function cari(Request $request){
        $title = $request->keywod;
        $key = $request->keywod;
        $produk = Product::where('nama','like','%'.$key.'%')->where('status',1)->paginate(15);
        $hitung = count(\App\Models\Product::where('nama','like','%'.$key.'%')->where('status',1)->get());
        // if($request->kategori_id == 0){
        //     $kategori_id = $request->kategori_id;
        //     $data = Product::where('kategori_id',$kategori_id)->where('nama','like','%'.$keyword.'%')->where('status',1)->get();
        // }else{
        //     $data = Product::where('nama','like','%'.$keyword.'%')->where('status',1)->get();
        // }

        return view('beranda.home',compact('title','produk','hitung'));
    }
}
