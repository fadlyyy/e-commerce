<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Pesanan;
use App\Models\Pesanan_barang;
use App\Models\Pesanan_alamat;

use Cart;
use DB;

class Cart_controller extends Controller
{
    public function index(){
        $title = 'Sangcahaya.com | Keranjang';
        
        return view('beranda.cart',compact('title'));
    }
    
    public function remove(){
        Cart::destroy();
        
        \Session::flash('pesan','Keranjang di kosongkan');
        return redirect()->back();
    }
    
    public function add(Request $request, $id){
        $pr = Product::where('product_id',$id)->first();
        
    	$product_id = $id;
    	$nama = $pr->nama;
    	$qty = $request->qty;
    	$harga = $pr->harga_akhir;
    	$warna_id = $request->warna_id;
    	$ukuran_id = $request->ukuran_id;
    	
    	Cart::add([
    	    'id'=>$product_id,
    	    'name'=>$nama,
    	    'qty'=>$qty,
    	    'price'=>$harga,
    	    'options'=>['warna_id'=>$warna_id,'ukuran_id'=>$ukuran_id,'berat'=>$pr->berat]
    	]);
    	
    	\Session::flash('pesan','Berhasil di masukkan ke Keranjang');
    	return redirect()->back();
    }
    
    public function ongkir(){
        $title = 'Sangcahaya.com | Cek Ongkir';
        
        $curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: a35bcf69df327236675d8180b9449930"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if($err){
			dd($err);
		}else{
			$provinsi = json_decode($response);
		}
		

        $berat = 0;
        foreach(\Cart::content() as $cr){
            $qty = $cr->qty;
            $brt = $cr->options->berat;
            $berat += $qty * $brt;
        }
        
		return view('beranda.ongkir',compact('title','provinsi','berat'));
    }
    
    public function kota($provinsi){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=$provinsi",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: a35bcf69df327236675d8180b9449930"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$kota = json_decode($response);
			// dd($kota);
		}

		return response()->json([
			'data'=>$kota
		]);
	}

	public function cek(Request $request){
		$kota_asal = $request->kota_asal;
		$kota_tujuan = $request->kota_tujuan;
		$kurir = $request->kurir;
		$berat = $request->berat;
		$group_id = $request->group_id;

		// \DB::table('ongkir')->where('group_id',$group_id)->update([
		// 	'harga'=>
		// ])

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=".$kota_asal."&destination=".$kota_tujuan."&weight=".$berat."&courier=".$kurir."",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: a35bcf69df327236675d8180b9449930"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {	  echo "cURL Error #:" . $err;
		} else {
			$data = json_decode($response);
		}

		return response()->json([
			'hasil'=>$data
		]);
	}

	public function bayar(Request $request){
		$this->validate($request,[
			'nama1'=>'required',
			'nama2'=>'required',
			'provinsi1'=>'required',
			'provinsi2'=>'required',
			'kota1'=>'required',
			'kota2'=>'required',
			'kurir'=>'required',
			'kode_pos'=>'required',
			'nope'=>'required',
			'alamat'=>'required',
			'layanan'=>'required',
		]);

		DB::transaction(function() use($request){

			$id = \Uuid::generate(4);
			$totalharga = 0;
			$layanan = '';
			$ongkir = 0;

			foreach (\Cart::content() as $cr) {
				$totalharga += $cr->price * $cr->qty;
			}

			$layananNya = explode('-', $request->layanan);
			// dd($layananNya);
			$layanan = $layananNya[0];
			$ongkir = $layananNya[1];

			Pesanan::insert([
				'pesanan_id'=>$id,
				'tanggal'=>date('Y-m-d H:i:s'),
				'user_id'=>\Auth::user()->id,
				'total_harga'=>$totalharga,
				'status'=>2	
			]);

			Pesanan_alamat::insert([
				'pesanan_detail_id'=>\Uuid::generate(4),
				'pesanan_id'=>$id,
				'nama1'=>$request->nama1,
				'nama2'=>$request->nama2,
				'provinsi1'=>$request->provinsi1,
				'provinsi2'=>$request->provinsi2,
				'kota1'=>$request->kota1,
				'kota2'=>$request->kota2,
				'kurir'=>$request->kurir,
				'kode_pos'=>$request->kode_pos,
				'nope'=>$request->nope,
				'alamat'=>$request->alamat,
				'layanan'=>$layanan,
				'ongkir'=>$ongkir,
			]);

			$qty = 0;
			$subharga = 0;
			$warna_id = '';
			$ukuran_id = '';
			$berat = 0;

			foreach (\Cart::content() as $cr) {
				$qty = $cr->qty;
				$price = $cr->price;
				$warna_id = $cr->options['warna_id'];
				$ukuran_id = $cr->options['ukuran_id'];
				$berat = $cr->options['berat'];

				Pesanan_barang::insert([
					'pesanan_barang_id'=>\Uuid::generate(4),
					'pesanan_id'=>$id,
					'product_id'=>$cr->id,
					'qty'=>$qty,
					'harga'=>$price,
					'subharga'=>$qty*$price,
					'warna_id'=>$warna_id,
					'ukuran_id'=>$ukuran_id,
					'berat'=>$berat
				]);
			}

		});

		return redirect('bayar/guide');
	}

	public function bayar_guide(){
		$title = 'Petunjuk Pembayaran';

		return view('beranda.pembayaran',compact('title'));
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
